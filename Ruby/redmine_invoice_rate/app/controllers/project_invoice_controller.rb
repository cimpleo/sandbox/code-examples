class ProjectInvoiceController < ApplicationController
  unloadable

  before_filter :auth, :only => [ :new ]

  before_filter :find_project_by_project_id, :only => [ :new ]

  def new

    @invoice = Invoice.new
    @invoice.number = Invoice.generate_invoice_number(@project)
    @invoice.invoice_date = Date.today
    @invoice.contact = Contact.find_by_id(params[:contact_id]) if params[:contact_id]
    @invoice.assigned_to = User.current
    @invoice.currency ||= ContactsSetting.default_currency
    # Custom for context_menu action
    if params[:issues_ids]
      @invoice.amount = 0.0;
      scope = Issue.scoped(:include => :time_entries,  :conditions => {:id => params[:issues_ids]})
      scope.all.each do |issue|
        if user = ProjectBilling.find_by_project_id( @project.id ).project_user_prices.find_by_user_id( issue.assigned_to_id )
          price = user.price
        end

        @invoice.lines << InvoiceLine.new(
          :description  => issue.subject, 
          :quantity     => issue.spent_hours || 0.0 , 
          :price        => price || 0.0,
          :units        => issue.spent_hours == 1.0 ? 'hr' : 'hrs',
        )
        @invoice.amount += price*issue.spent_hours
      end

    end
    #EndCustom
    @invoice.lines.build if @invoice.lines.blank?

    @last_invoice_number = Invoice.last.try(:number)

  end

  def auth
    @project = Project.find(params[:project_id])
    if !User.current.allowed_to?(:redmine_invoice_rate, @project)
      render_404
    end
  end

end
