class ProjectBilling < ActiveRecord::Base
	unloadable

	has_many :project_user_prices
	belongs_to	:project, :class_name => "Project", :foreign_key => "project_id"
end
