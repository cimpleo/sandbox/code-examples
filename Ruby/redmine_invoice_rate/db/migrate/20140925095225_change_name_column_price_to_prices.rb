class ChangeNameColumnPriceToPrices < ActiveRecord::Migration
  def up
  	rename_column :project_billings, :price, :prices
  end

  def down
  	rename_column :project_billings, :prices, :price
  end
end
