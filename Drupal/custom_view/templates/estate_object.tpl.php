<?php
    //$variables['title_attributes'] = array('myvalue' => 'df');
    //krumo($variables);
    $estateObject = $variables['object'];
    $path = $variables['path'];
    $images = $variables['images'];
    $filesUrl = variable_get('file_public_path', conf_path() . '/files');
    $i = 0;
?>

<?php if ($images):?>
<div class="carousel">
    <?php foreach ($images as $image): ?>
    <?php       
        $i = $i % 2 + 1;           
        //$imageUrl = image_style_url('estate_list_style', $filesUrl.'/estate-images/'.$i.'.jpg');
        $imageUrl = theme('imagecache_external', array('path' => 'http://www.previews.ch/sites/previews.ch/files/'.$i.'.jpg', 'style_name'=> 'estate_list')); 
    ?>
    <div class="slide"><?php print $imageUrl; ?></div>
    <?php endforeach;?>
</div>
<?php endif; ?>
<div class="object-wrapper">
    
    <div class="agent-details">
        <div class="agent-sort">
            <?php print t('Sort by:'); ?>
        </div>

        <div class="agent-name">
            <a class="agent-data" href="/property/<?php print $estateObject->DSN; ?>/spec"><?php print t($estateObject->Webserver___Vor_Nachname.' >'); unset($estateObject->Webserver___Vor_Nachname); ?></a>
        </div>

        <div class="desk">
            <?php print t($estateObject->Webserver___Buero); unset($estateObject->Webserver___Buero); ?>
        </div>

        <div class="street">
            <?php print t($estateObject->Webserver___Strasse); unset($estateObject->Webserver___Strasse); ?>
        </div>

        <div class="place">
            <?php print t($estateObject->Webserver___PLZ_Ort); unset($estateObject->Webserver___PLZ_Ort); ?>
        </div>

        <div class="phone">
            <label><?php print t('Phone:'); ?></label><?php print t($estateObject->Webserver___Telefon); unset($estateObject->Webserver___Telefon); ?>
        </div>

        <div class="fax">
            <label><?php print t('Fax:'); ?></label><?php print t($estateObject->Webserver___Telefax); unset($estateObject->Webserver___Telefax); ?>
        </div>

    </div>

    <div class="property-details">

        <div class="internal">
            <?php print t($estateObject->INTERN); unset($estateObject->INTERN); ?>      
        </div>

        <div class="place">
            <?php print t($estateObject->Land_PLZ_Ort); unset($estateObject->Land_PLZ_Ort); ?>
        </div>

        <div class="cost">
            <label><?php print t('&#8364;'); ?></label><?php print t(($estateObject->Kaufpreis) ? $estateObject->Kaufpreis : $estateObject->Miete); unset($estateObject->Kaufpreis); unset($estateObject->Miete);?>
        </div>

        <div class="room">
            <?php print t($estateObject->Zimmer); unset($estateObject->Zimmer); ?><label><?php print t('Rooms'); ?></label>
        </div>

        <div class="live-area">
            <?php print t($estateObject->Wohnflaeche); unset($estateObject->Wohnflaeche); ?><label><?php print t('Sq Feet'); ?></label>
        </div>

        <div class="desc">
            <label><?php print t('Description:'); ?></label><?php print t($estateObject->TEXTOBJEKT); unset($estateObject->TEXTOBJEKT); ?>
        </div>

        <div class="equipment">
            <label><?php print t('Equipment:'); ?></label><?php print t($estateObject->TEXTAUSSTATTUNG); unset($estateObject->TEXTAUSSTATTUNG); ?>
        </div>

        <div class="location">
            <label><?php print t('Location:'); ?></label><?php print t($estateObject->TEXTLAGE); unset($estateObject->TEXTLAGE); ?>
        </div>

        <div class="toolbar">
            
        </div>
        
        <div class="features">
            <label class="features-label"><?php print t('Features:'); ?></label>
                <?php
                    $estateObject = (array)$estateObject;

                    foreach ($estateObject as $key => $value) {
                        if ($value) {
                            print_r('<div class="feature"><label>'.t($key).':</label><div class='.$key.'>'.t($value).'</div></div>');
                        }
                    }
                ?>
        </div>

        <div id="gmap">
            <?php //print gmap_simple_map($estateObject['LAT'], $estateObject['LNG'], 'coldwell','', 25, '641px', '460px', FALSE,''); ?>
        </div>

    </div>
    
</div>    
<?php 
    drupal_add_js('http://maps.googleapis.com/maps/api/js?key=AIzaSyCNDcb176WU04y2VIDhuPb3eGCb5UxSqi0&sensor=false', 'external');
    drupal_add_js(array('customViewModule' => array('lat' => $estateObject['LAT'], 'lng' => $estateObject['LNG'], 'path' => $path)), 'setting');
    drupal_add_js($path.'/templates/js/gmap.js');
    drupal_add_js($path.'/templates/js/bxslider.js');
    drupal_add_js($path.'/templates/js/object-images-slider.js');
?>