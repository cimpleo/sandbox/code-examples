(function ($) {

  Drupal.behaviors.estateAgentContactForms = {
    attach: function (context, settings) {    
        
        var object = Drupal.settings.estateAgentInfo;
        var start;
        var end;
        
        function onAjaxSuccess(data) {
            start = data.indexOf('<div class="info-sidebar">');
            end = data.indexOf('</form>')+7;
            $('#agent-dialog').html(data.substr(start, end-start));            
        }
          
        $('.contact-link a').live('click', function(e){
            e.preventDefault();
            dsn = $(this).attr('href');
            
            $.post(
                "/agent/webform",
                {
                    type: 'js_agent_form',
                    call: 'estate-list-contact', 
                    email: object.Webserver___EMail_P,
                    intern: object.INTERN,
                    address: object.buero,
                    price: object.euro + object.expression,
                    agent: object.ip_usr_vorname + ' ' + object.ip_usr_nachname,
                    fax: object.ip_usr_telefax,
                    phone: object.phone,
                },
                onAjaxSuccess
            );
            
            $('#agent-dialog').dialog({modal:true});
            
        });
        
        
        
    }    
  };

})(jQuery);