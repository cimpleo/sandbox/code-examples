(function ($) {
    Drupal.behaviors.tabMapProperty = {
        attach: function (context, settings) {
            function initialize_big_map() {   
                var coordinates=Drupal.settings.property_coord;
              
                var myLatlng = new google.maps.LatLng(coordinates.LAT, coordinates.LNG);
                var marker;
                var myOptions = {
                    zoom: 17,
                    center: myLatlng,
                    mapTypeId: google.maps.MapTypeId.SATELLITE
                }                
                var map = new google.maps.Map(document.getElementById("map"), myOptions); 
                marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map    
                });
            }   
            function initialize_small_map(){            
                var coordinates=Drupal.settings.property_coord;
                var myLatlng = new google.maps.LatLng(coordinates.LAT, coordinates.LNG);
                var marker;
                var myOptions = {
                    zoom: 17,
                    center: myLatlng,
                    mapTypeId: google.maps.MapTypeId.SATELLITE
                }       
                
                var small_map  = new google.maps.Map(document.getElementById("small-map"), myOptions); 
                marker = new google.maps.Marker({
                    position: myLatlng,
                    map: small_map, 
                    icon: Drupal.settings.path+'/images/icon.png'
                });
            }  
            var $tabs = $("#tabs").tabs();
                               
            initialize_small_map();
            $('a[href="#tabs-2"]').click(function(){   
                                        
                if ($('#map').children().length==0){
                    initialize_big_map(); 
                }  
            });  
            $('.view-map').click(function() {
                 $("html, body").animate({
                    scrollTop: 0
                }, 600);
                $tabs.tabs("option", "active", 1); 
                if (($('#map').children().length==0)&&($("#tabs").tabs('option', 'active')==1)){
                    initialize_big_map();                     
                } 
                return false;
            });           
        }
    };
})(jQuery);
