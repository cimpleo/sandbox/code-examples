<?php
/**
 * @file
 * compliance_training.page.inc
 */

/**
 * Landing page for Risk Areas.
 */
function compliance_training_training_landing($term) {
  // $term may be a tid; if it isn't try to get the tid from name.
  if (is_numeric($term)) {
    $tid = $term;
  } else {
    $term = taxonomy_get_term_by_name($term, 'training_type');
    if (empty($term)) {
      return '<i>An associated term could not be found.</i>';
    } else {
      $tid = $term->tid;
    }
  }
  
  if (empty($tid)) {
    return '<i>No Term data was found.</i>';
  }
  
  if (empty($term->name)) {
    $term = taxonomy_term_load($tid);
  }
  
  // Now EFQ.
  $efq = new EntityFieldQuery();
  $efq->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'training')
    ->propertyCondition('status', NODE_PUBLISHED)
    ->propertyOrderBy('created', 'DESC')
    ->fieldCondition('field_training_type', 'tid', $tid)
    ->addTag('compliance_training_training_landing');
  
  $results = $efq->execute();
  
  $keys = array_keys($results['node']);
  $entities = node_load_multiple($keys);
  $nodes = node_view_multiple($entities, 'teaser');
  $nodes = $nodes['nodes'];
  $page = '<h1>Trainings: '.$term->name.'</h1>';
  $page .= render($nodes);
  return $page;
}