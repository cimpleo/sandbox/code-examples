<?php
/**
 * @file
 * joc_aro.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function joc_aro_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-aro_block_archive_years'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'machine_name' => 'aro_block_archive_years',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
      'joc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'joc',
        'weight' => 0,
      ),
      'joc_bootstrap' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'joc_bootstrap',
        'weight' => 0,
      ),
      'microsite_default' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'microsite_default',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['block-aro_block_comments_index'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'machine_name' => 'aro_block_comments_index',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
      'joc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'joc',
        'weight' => 0,
      ),
      'joc_bootstrap' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'joc_bootstrap',
        'weight' => 0,
      ),
      'microsite_default' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'microsite_default',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['block-aro_block_content_aro'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'machine_name' => 'aro_block_content_aro',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
      'joc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'joc',
        'weight' => 0,
      ),
      'joc_bootstrap' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'joc_bootstrap',
        'weight' => 0,
      ),
      'microsite_default' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'microsite_default',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['block-aro_block_content_aro_perspectiv'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'machine_name' => 'aro_block_content_aro_perspectiv',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
      'joc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'joc',
        'weight' => 0,
      ),
      'joc_bootstrap' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'joc_bootstrap',
        'weight' => 0,
      ),
      'microsite_default' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'microsite_default',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['block-aro_block_search_company'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'machine_name' => 'aro_block_search_company',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
      'joc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'joc',
        'weight' => 0,
      ),
      'joc_bootstrap' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'joc_bootstrap',
        'weight' => 0,
      ),
      'microsite_default' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'microsite_default',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['block-aro_block_search_lname'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'machine_name' => 'aro_block_search_lname',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
      'joc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'joc',
        'weight' => 0,
      ),
      'joc_bootstrap' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'joc_bootstrap',
        'weight' => 0,
      ),
      'microsite_default' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'microsite_default',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
