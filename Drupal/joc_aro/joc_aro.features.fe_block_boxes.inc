<?php
/**
 * @file
 * joc_aro.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function joc_aro_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'ARO Landing Page Archive Years';
  $fe_block_boxes->format = '3';
  $fe_block_boxes->machine_name = 'aro_block_archive_years';
  $fe_block_boxes->body = '<?php
$tid = "";
try {
  $term = menu_get_object(\'taxonomy_term\', 2);
  $tid = "/".$term->tid; 
}
catch (Exception $e) {}
print \'<div class="aro-category-sidebar-archive"><span class="aro-category-sidebar-title">Archived Years</span> \';
switch ($term->name) {

// LINKS STARTS HERE

  case \'Air Cargo\':
    print \'<a href="http://www.joc.com/special-topics/2014-annual-review-outlook-air-cargo">2014</a> <a href="http://www.joc.com/special-topics/2013-annual-review-outlook-air-cargo">2013</a>\';
  break;
  case \'Maritime\':
    print \'<a href="http://www.joc.com/special-topics/2014-annual-review-outlook-maritime">2014</a> <a href="http://www.joc.com/special-topics/2013-annual-review-outlook-maritime">2013</a>\';
  break;
  case \'Trucking\':
    print \'<a href="http://www.joc.com/special-topics/2014-annual-review-outlook-trucking">2014</a> <a href="http://www.joc.com/special-topics/2013-annual-review-outlook-trucking">2013</a>\';
  break;
  case \'Rail & Intermodal\':
    print \'<a href="http://www.joc.com/special-topics/2014-annual-review-outlook-rail-intermodal">2014</a> <a href="http://www.joc.com/special-topics/2013-annual-review-outlook-rail-intermodal">2013</a>\';
  break;
  case \'Logistics\':
    print \'<a href="http://www.joc.com/special-topics/2014-annual-review-outlook-logistics">2014</a> <a href="http://www.joc.com/special-topics/2013-annual-review-outlook-logistics">2013</a>\';
  break;
  case \'Government\':
    print \'<a href="http://www.joc.com/special-topics/2014-annual-review-outlook-government">2014</a> <a href="http://www.joc.com/special-topics/2013-annual-review-outlook-government">2013</a>\';
  break;

// LINKS ENDS HERE

}
print \'</div>\';
?>';

  $export['aro_block_archive_years'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'ARO Landing Page Comments index';
  $fe_block_boxes->format = '3';
  $fe_block_boxes->machine_name = 'aro_block_comments_index';
  $fe_block_boxes->body = '<?php
$tid = "";
try {
  $term = menu_get_object(\'taxonomy_term\', 2);
  $tid = "/".$term->tid;
}
catch (Exception $e) {}
?>
<div class="aro-category-sidebar-comments">
<span class="aro-category-sidebar-title">Comments Index</span> <a href="/aro-search-author-company<?php print $tid; ?>">Alphabetical by Company</a> <a href="/aro-search-author-last-name<?php print $tid; ?>">Alphabetical by Author Last Name</a>
</div>';

  $export['aro_block_comments_index'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'ARO Content aro';
  $fe_block_boxes->format = 'wysiwyg';
  $fe_block_boxes->machine_name = 'aro_block_content_aro';
  $fe_block_boxes->body = '<p>ANNUAL REVIEW &amp; OUTLOOK, 2015</p>';

  $export['aro_block_content_aro'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'ARO Content aro perspective';
  $fe_block_boxes->format = 'wysiwyg';
  $fe_block_boxes->machine_name = 'aro_block_content_aro_perspectiv';
  $fe_block_boxes->body = '<p>ANNUAL REVIEW &amp; OUTLOOK, PERSPECTIVE, 2015</p>';

  $export['aro_block_content_aro_perspectiv'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'ARO Search Company Text';
  $fe_block_boxes->format = 'wysiwyg';
  $fe_block_boxes->machine_name = 'aro_block_search_company';
  $fe_block_boxes->body = '<p>To search ARO Commentaries by company, start typing the company name in the box below and click submit. Results will be displayed in a list below.</p>';

  $export['aro_block_search_company'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'ARO Search Last Name Text';
  $fe_block_boxes->format = 'wysiwyg';
  $fe_block_boxes->machine_name = 'aro_block_search_lname';
  $fe_block_boxes->body = '<p>To search ARO Commentaries by author\'s last name, start typing on the box below and click submit. Results will be displayed in a list below.</p>';

  $export['aro_block_search_lname'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'ARO Terms block';
  $fe_block_boxes->format = 'wysiwyg';
  $fe_block_boxes->machine_name = 'aro_block_terms';
  $fe_block_boxes->body = '<p><span style="color: #222222; font-family: serif; font-size: 16px; line-height: 22.3999996185303px;">The January 2015 edition of the JOC Annual Review &amp; Outlook contributes an industry barometer and analysis of the challenging year ahead including changes that may be coming in the broader economy, in the marketplace or in the regulatory world that will affect business in 2015 including Air Cargo, Government,&nbsp;</span><span style="color: #222222; font-family: serif; font-size: 16px; line-height: 22.3999996185303px;">Logistics,&nbsp;</span><span style="color: #222222; font-family: serif; font-size: 16px; line-height: 22.3999996185303px;">Maritime, Rail and Intermodal and&nbsp;</span><span style="color: #222222; font-family: serif; font-size: 16px; line-height: 22.3999996185303px;">Trucking</span><span style="color: #222222; font-family: serif; font-size: 16px; line-height: 22.3999996185303px;">&nbsp;sectors.</span></p>
<p><span style="color: #222222; font-family: serif; font-size: 16px; line-height: 22.3999996185303px;">Select a category below to view more content or search via author\'s last name in the search box above.</span></p>';

  $export['aro_block_terms'] = $fe_block_boxes;

  return $export;
}
