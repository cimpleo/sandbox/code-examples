<?php
/**
 * @file
 * joc_articles.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function joc_articles_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view_panel_context';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -25;
  $handler->conf = array(
    'title' => 'Articles',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(
      0 => array(
        'identifier' => 'ARO Term',
        'keyword' => 'aro_term',
        'name' => 'entity_from_field:field_aro-node-taxonomy_term',
        'delta' => 0,
        'context' => 'argument_entity_id:node_1',
        'id' => 1,
      ),
    ),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'article' => 'article',
              'data_story' => 'data_story',
              'commentary' => 'commentary',
              'page' => 'page',
              'podcast' => 'podcast',
              'press_release' => 'press_release',
              'reviews' => 'reviews',
              'video' => 'video',
              'webcast' => 'webcast',
              'whitepaper' => 'whitepaper',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
        1 => array(
          'name' => 'theme',
          'settings' => array(
            'theme' => 'joc',
          ),
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
    'panels_breadcrumbs_state' => 1,
    'panels_breadcrumbs_titles' => 'JOC',
    'panels_breadcrumbs_paths' => '<front>',
    'panels_breadcrumbs_home' => 0,
  );
  $display = new panels_display();
  $display->layout = '1col-sidebar';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'sidebar' => NULL,
    ),
    'sidebar' => array(
      'style' => '-1',
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'a45a1ee4-a8da-4c06-a339-f4559f14af4d';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-ce5de332-8324-4173-8776-b0b7d03ab71d';
    $pane->panel = 'center';
    $pane->type = 'page_breadcrumb';
    $pane->subtype = 'page_breadcrumb';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'php',
          'settings' => array(
            'description' => 'Main Category',
            'php' => 'return empty($contexts[\'argument_entity_id:node_1\']->data->taxonomy_vocabulary_1);',
          ),
          'not' => TRUE,
        ),
      ),
    );
    $pane->configuration = array(
      'panels_breadcrumbs_admin_title' => '',
      'panels_breadcrumbs_state' => 0,
      'panels_breadcrumbs_titles' => '',
      'panels_breadcrumbs_paths' => '',
      'panels_breadcrumbs_home' => 0,
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array(
      'method' => 'simple',
      'settings' => array(
        'lifetime' => '300',
        'granularity' => 'args',
      ),
    );
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'ce5de332-8324-4173-8776-b0b7d03ab71d';
    $display->content['new-ce5de332-8324-4173-8776-b0b7d03ab71d'] = $pane;
    $display->panels['center'][0] = 'new-ce5de332-8324-4173-8776-b0b7d03ab71d';
    $pane = new stdClass();
    $pane->pid = 'new-c226c1ed-0077-410b-8c74-7358ad201d25';
    $pane->panel = 'center';
    $pane->type = 'page_breadcrumb';
    $pane->subtype = 'page_breadcrumb';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'php',
          'settings' => array(
            'description' => 'Main Category',
            'php' => 'return empty($contexts[\'argument_entity_id:node_1\']->data->taxonomy_vocabulary_1);',
          ),
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'panels_breadcrumbs_admin_title' => '',
      'panels_breadcrumbs_state' => 1,
      'panels_breadcrumbs_titles' => 'JOC
ARO
%node:field-aro',
      'panels_breadcrumbs_paths' => '<front>
aro
%aro_term:url',
      'panels_breadcrumbs_home' => 0,
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'c226c1ed-0077-410b-8c74-7358ad201d25';
    $display->content['new-c226c1ed-0077-410b-8c74-7358ad201d25'] = $pane;
    $display->panels['center'][1] = 'new-c226c1ed-0077-410b-8c74-7358ad201d25';
    $pane = new stdClass();
    $pane->pid = 'new-8730ed0d-2250-4c6e-a2fa-f4d8773cabac';
    $pane->panel = 'center';
    $pane->type = 'page_title';
    $pane->subtype = 'page_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'markup' => 'h1',
      'class' => '',
      'id' => 'page-title',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array(
      'method' => '0',
      'settings' => array(),
    );
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '8730ed0d-2250-4c6e-a2fa-f4d8773cabac';
    $display->content['new-8730ed0d-2250-4c6e-a2fa-f4d8773cabac'] = $pane;
    $display->panels['center'][2] = 'new-8730ed0d-2250-4c6e-a2fa-f4d8773cabac';
    $pane = new stdClass();
    $pane->pid = 'new-832487d7-d4a3-47c9-bf18-c1f16312fb17';
    $pane->panel = 'center';
    $pane->type = 'node_body';
    $pane->subtype = 'node_body';
    $pane->shown = FALSE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'google_first_click_free',
          'settings' => NULL,
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
        1 => array(
          'name' => 'joc_user_metering',
          'settings' => NULL,
          'not' => FALSE,
        ),
      ),
      'logic' => 'or',
    );
    $pane->configuration = array(
      'context' => 'argument_entity_id:node_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '832487d7-d4a3-47c9-bf18-c1f16312fb17';
    $display->content['new-832487d7-d4a3-47c9-bf18-c1f16312fb17'] = $pane;
    $display->panels['center'][3] = 'new-832487d7-d4a3-47c9-bf18-c1f16312fb17';
    $pane = new stdClass();
    $pane->pid = 'new-214ccc9a-ed35-4721-ab4e-4b4897d29998';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:taxonomy_vocabulary_1';
    $pane->shown = FALSE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'hs_taxonomy_term_reference_hierarchical_links',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '214ccc9a-ed35-4721-ab4e-4b4897d29998';
    $display->content['new-214ccc9a-ed35-4721-ab4e-4b4897d29998'] = $pane;
    $display->panels['center'][4] = 'new-214ccc9a-ed35-4721-ab4e-4b4897d29998';
    $pane = new stdClass();
    $pane->pid = 'new-c278f248-30d3-4cc8-9786-17a141da6edc';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_byline';
    $pane->shown = FALSE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'php',
          'settings' => array(
            'description' => 'Check if ARO',
            'php' => 'return empty($contexts[\'argument_entity_id:node_1\']->data->field_aro);',
          ),
          'not' => TRUE,
        ),
      ),
    );
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = 'c278f248-30d3-4cc8-9786-17a141da6edc';
    $display->content['new-c278f248-30d3-4cc8-9786-17a141da6edc'] = $pane;
    $display->panels['center'][5] = 'new-c278f248-30d3-4cc8-9786-17a141da6edc';
    $pane = new stdClass();
    $pane->pid = 'new-b753df24-7905-40aa-b031-3576879dbf31';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_sub_headline';
    $pane->shown = FALSE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'php',
          'settings' => array(
            'description' => 'ARO Check',
            'php' => 'return empty($contexts[\'argument_entity_id:node_1\']->data->field_aro);',
          ),
          'not' => TRUE,
        ),
      ),
    );
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'aro-field-deck',
    );
    $pane->extras = array();
    $pane->position = 6;
    $pane->locks = array();
    $pane->uuid = 'b753df24-7905-40aa-b031-3576879dbf31';
    $display->content['new-b753df24-7905-40aa-b031-3576879dbf31'] = $pane;
    $display->panels['center'][6] = 'new-b753df24-7905-40aa-b031-3576879dbf31';
    $pane = new stdClass();
    $pane->pid = 'new-ef09680f-a5d4-4b64-87b0-2fa8ec859ff0';
    $pane->panel = 'center';
    $pane->type = 'block';
    $pane->subtype = 'block-171';
    $pane->shown = FALSE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'php',
          'settings' => array(
            'description' => 'ARO check',
            'php' => 'return empty($contexts[\'argument_entity_id:node_1\']->data->field_aro);',
          ),
          'not' => TRUE,
        ),
      ),
    );
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 7;
    $pane->locks = array();
    $pane->uuid = 'ef09680f-a5d4-4b64-87b0-2fa8ec859ff0';
    $display->content['new-ef09680f-a5d4-4b64-87b0-2fa8ec859ff0'] = $pane;
    $display->panels['center'][7] = 'new-ef09680f-a5d4-4b64-87b0-2fa8ec859ff0';
    $pane = new stdClass();
    $pane->pid = 'new-3fb67b29-027c-4f26-a265-e4faf95e781b';
    $pane->panel = 'center';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'google_first_click_free',
          'settings' => NULL,
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
        1 => array(
          'name' => 'joc_user_metering',
          'settings' => NULL,
          'not' => FALSE,
        ),
      ),
      'logic' => 'or',
    );
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'argument_entity_id:node_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array(
      'method' => '0',
      'settings' => array(),
    );
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 8;
    $pane->locks = array();
    $pane->uuid = '3fb67b29-027c-4f26-a265-e4faf95e781b';
    $display->content['new-3fb67b29-027c-4f26-a265-e4faf95e781b'] = $pane;
    $display->panels['center'][8] = 'new-3fb67b29-027c-4f26-a265-e4faf95e781b';
    $pane = new stdClass();
    $pane->pid = 'new-2fdac2a4-dc84-464a-8de8-e17e2bc515bd';
    $pane->panel = 'center';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'google_first_click_free',
          'settings' => NULL,
          'context' => 'argument_entity_id:node_1',
          'not' => TRUE,
        ),
        1 => array(
          'name' => 'joc_user_metering',
          'settings' => NULL,
          'not' => TRUE,
        ),
      ),
      'logic' => 'and',
    );
    $pane->configuration = array(
      'view_mode' => 'teaser',
      'context' => 'argument_entity_id:node_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array(
      'method' => '0',
      'settings' => array(),
    );
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 9;
    $pane->locks = array();
    $pane->uuid = '2fdac2a4-dc84-464a-8de8-e17e2bc515bd';
    $display->content['new-2fdac2a4-dc84-464a-8de8-e17e2bc515bd'] = $pane;
    $display->panels['center'][9] = 'new-2fdac2a4-dc84-464a-8de8-e17e2bc515bd';
    $pane = new stdClass();
    $pane->pid = 'new-6ef5052a-895f-4f64-93af-5fda20689abc';
    $pane->panel = 'center';
    $pane->type = 'block';
    $pane->subtype = 'joctweaks-content_to_continue';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'google_first_click_free',
          'settings' => NULL,
          'context' => 'argument_entity_id:node_1',
          'not' => TRUE,
        ),
        1 => array(
          'name' => 'joc_user_metering',
          'settings' => NULL,
          'not' => TRUE,
        ),
      ),
      'logic' => 'and',
    );
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array(
      'method' => '0',
      'settings' => array(),
    );
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 10;
    $pane->locks = array();
    $pane->uuid = '6ef5052a-895f-4f64-93af-5fda20689abc';
    $display->content['new-6ef5052a-895f-4f64-93af-5fda20689abc'] = $pane;
    $display->panels['center'][10] = 'new-6ef5052a-895f-4f64-93af-5fda20689abc';
    $pane = new stdClass();
    $pane->pid = 'new-a3a334e2-3146-4db3-938a-cdf07123cd4b';
    $pane->panel = 'center';
    $pane->type = 'block';
    $pane->subtype = 'user-login';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 1,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => '',
    );
    $pane->extras = array();
    $pane->position = 11;
    $pane->locks = array();
    $pane->uuid = 'a3a334e2-3146-4db3-938a-cdf07123cd4b';
    $display->content['new-a3a334e2-3146-4db3-938a-cdf07123cd4b'] = $pane;
    $display->panels['center'][11] = 'new-a3a334e2-3146-4db3-938a-cdf07123cd4b';
    $pane = new stdClass();
    $pane->pid = 'new-7491cbfb-7162-48df-8a6b-c56fad1f22b8';
    $pane->panel = 'center';
    $pane->type = 'views_panes';
    $pane->subtype = 'more_breaking_news-panel_pane_2';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'arguments' => array(
        'taxonomy_vocabulary_1_tid' => '%node:taxonomy-vocabulary-1:tid',
      ),
      'fields_override' => array(
        'field_feature_image' => 1,
        'title' => 1,
      ),
      'override_title' => 1,
      'override_title_text' => 'More Breaking %node:taxonomy-vocabulary-1 News',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 12;
    $pane->locks = array();
    $pane->uuid = '7491cbfb-7162-48df-8a6b-c56fad1f22b8';
    $display->content['new-7491cbfb-7162-48df-8a6b-c56fad1f22b8'] = $pane;
    $display->panels['center'][12] = 'new-7491cbfb-7162-48df-8a6b-c56fad1f22b8';
    $pane = new stdClass();
    $pane->pid = 'new-2121f383-576f-40b1-8b77-dac4083d0efd';
    $pane->panel = 'center';
    $pane->type = 'views_panes';
    $pane->subtype = 'more_breaking_news-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => array(
        0 => 'argument_entity_id:node_1',
      ),
      'override_title' => 0,
      'override_title_text' => 'More popular news from %node:taxonomy-vocabulary-1',
      'override_title_heading' => 'h2',
      'path' => '',
      'arguments' => array(
        'taxonomy_vocabulary_1_tid' => '%node:taxonomy-vocabulary-1:tid',
      ),
      'fields_override' => array(
        'title' => 1,
      ),
    );
    $pane->cache = array(
      'method' => '0',
      'settings' => array(),
    );
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 13;
    $pane->locks = array();
    $pane->uuid = '2121f383-576f-40b1-8b77-dac4083d0efd';
    $display->content['new-2121f383-576f-40b1-8b77-dac4083d0efd'] = $pane;
    $display->panels['center'][13] = 'new-2121f383-576f-40b1-8b77-dac4083d0efd';
    $pane = new stdClass();
    $pane->pid = 'new-374283ab-f988-4489-9848-4afd601b81af';
    $pane->panel = 'center';
    $pane->type = 'node_comment_form';
    $pane->subtype = 'node_comment_form';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'google_first_click_free',
          'settings' => NULL,
          'not' => FALSE,
        ),
        1 => array(
          'name' => 'joc_user_metering',
          'settings' => NULL,
          'not' => FALSE,
        ),
      ),
      'logic' => 'or',
    );
    $pane->configuration = array(
      'anon_links' => 0,
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array(
      'method' => '0',
      'settings' => array(),
    );
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 14;
    $pane->locks = array();
    $pane->uuid = '374283ab-f988-4489-9848-4afd601b81af';
    $display->content['new-374283ab-f988-4489-9848-4afd601b81af'] = $pane;
    $display->panels['center'][14] = 'new-374283ab-f988-4489-9848-4afd601b81af';
    $pane = new stdClass();
    $pane->pid = 'new-b29fd417-3c78-4974-9f5f-b514b2feffa2';
    $pane->panel = 'center';
    $pane->type = 'node_comments';
    $pane->subtype = 'node_comments';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'google_first_click_free',
          'settings' => NULL,
          'not' => FALSE,
        ),
        1 => array(
          'name' => 'joc_user_metering',
          'settings' => NULL,
          'not' => FALSE,
        ),
      ),
      'logic' => 'or',
    );
    $pane->configuration = array(
      'mode' => '1',
      'comments_per_page' => '300',
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array(
      'method' => 'simple',
      'settings' => array(
        'lifetime' => '60',
        'granularity' => 'args',
      ),
    );
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 15;
    $pane->locks = array();
    $pane->uuid = 'b29fd417-3c78-4974-9f5f-b514b2feffa2';
    $display->content['new-b29fd417-3c78-4974-9f5f-b514b2feffa2'] = $pane;
    $display->panels['center'][15] = 'new-b29fd417-3c78-4974-9f5f-b514b2feffa2';
    $pane = new stdClass();
    $pane->pid = 'new-070d6f3b-cfd5-428c-9969-8a7fe570dcde';
    $pane->panel = 'sidebar';
    $pane->type = 'block';
    $pane->subtype = 'joc_ads-ss_top';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'path_visibility',
          'settings' => array(
            'visibility_setting' => '0',
            'paths' => '*maersk*',
          ),
          'context' => 'empty',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '070d6f3b-cfd5-428c-9969-8a7fe570dcde';
    $display->content['new-070d6f3b-cfd5-428c-9969-8a7fe570dcde'] = $pane;
    $display->panels['sidebar'][0] = 'new-070d6f3b-cfd5-428c-9969-8a7fe570dcde';
    $pane = new stdClass();
    $pane->pid = 'new-d04712fc-25cb-4edb-9ff7-cba15958049c';
    $pane->panel = 'sidebar';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'path_visibility',
          'settings' => array(
            'visibility_setting' => '1',
            'paths' => '*maersk*',
          ),
          'context' => 'empty',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'admin_title' => 'JOC Custom Ad tag: Maersk 300x250',
      'title' => '',
      'body' => '<!-- begin ad tag (tile=2) -->
<script type="text/javascript">
//<![CDATA[
ord=Math.random()*10000000000000000;
document.write(\'<script type="text/javascript" src="http://ad.doubleclick.net/N9479/adj/joc.ubmgt/maersk;kw=maersk;tile=2;sz=300x250;ord=\' + ord + \'?"><\\/script>\');
//]]>
</script>
<noscript><a href="http://ad.doubleclick.net/N9479/jump/joc.ubmgt/maersk;kw=maersk;tile=2;sz=300x250;ord=123456789?" target="_blank" ><img src="http://ad.doubleclick.net/N9479/ad/joc.ubmgt/maersk;kw=maersk;tile=2;sz=300x250;ord=123456789?" border="0" alt="" /></a></noscript>
<!-- end ad tag -->',
      'format' => '2',
      'substitute' => 1,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'd04712fc-25cb-4edb-9ff7-cba15958049c';
    $display->content['new-d04712fc-25cb-4edb-9ff7-cba15958049c'] = $pane;
    $display->panels['sidebar'][1] = 'new-d04712fc-25cb-4edb-9ff7-cba15958049c';
    $pane = new stdClass();
    $pane->pid = 'new-3ea42b51-9149-493b-a0a6-b321a05fd430';
    $pane->panel = 'sidebar';
    $pane->type = 'views_panes';
    $pane->subtype = 'news-news_listing_pane';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'items_per_page' => '5',
      'arguments' => array(
        'taxonomy_vocabulary_1_tid' => '',
      ),
      'fields_override' => array(
        'field_feature_image' => 1,
        'field_access_level' => 1,
        'title' => 1,
        'taxonomy_vocabulary_1' => 1,
      ),
      'override_title' => 1,
      'override_title_text' => 'More on JOC',
    );
    $pane->cache = array(
      'method' => 'simple',
      'settings' => array(
        'lifetime' => '300',
        'granularity' => 'none',
      ),
    );
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '3ea42b51-9149-493b-a0a6-b321a05fd430';
    $display->content['new-3ea42b51-9149-493b-a0a6-b321a05fd430'] = $pane;
    $display->panels['sidebar'][2] = 'new-3ea42b51-9149-493b-a0a6-b321a05fd430';
    $pane = new stdClass();
    $pane->pid = 'new-f1a15e55-d3b7-4a1b-b02c-4acb93d17db2';
    $pane->panel = 'sidebar';
    $pane->type = 'block';
    $pane->subtype = 'joc_ads-ss_mid';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'f1a15e55-d3b7-4a1b-b02c-4acb93d17db2';
    $display->content['new-f1a15e55-d3b7-4a1b-b02c-4acb93d17db2'] = $pane;
    $display->panels['sidebar'][3] = 'new-f1a15e55-d3b7-4a1b-b02c-4acb93d17db2';
    $pane = new stdClass();
    $pane->pid = 'new-cc43fa4b-03c6-485c-b93a-d687f0b940c2';
    $pane->panel = 'sidebar';
    $pane->type = 'block';
    $pane->subtype = 'quicktabs-webcasts_podcasts_whitepapers';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<i>Recommended</i> Resources',
    );
    $pane->cache = array(
      'method' => 'simple',
      'settings' => array(
        'lifetime' => '300',
        'granularity' => 'none',
      ),
    );
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = 'cc43fa4b-03c6-485c-b93a-d687f0b940c2';
    $display->content['new-cc43fa4b-03c6-485c-b93a-d687f0b940c2'] = $pane;
    $display->panels['sidebar'][4] = 'new-cc43fa4b-03c6-485c-b93a-d687f0b940c2';
    $pane = new stdClass();
    $pane->pid = 'new-a711aa31-c3fd-4e9d-a275-331f58d28846';
    $pane->panel = 'sidebar';
    $pane->type = 'block';
    $pane->subtype = 'joc_ads-ss_btm';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = 'a711aa31-c3fd-4e9d-a275-331f58d28846';
    $display->content['new-a711aa31-c3fd-4e9d-a275-331f58d28846'] = $pane;
    $display->panels['sidebar'][5] = 'new-a711aa31-c3fd-4e9d-a275-331f58d28846';
    $pane = new stdClass();
    $pane->pid = 'new-4863f99f-36b8-4233-aba9-1e8314dc6c9e';
    $pane->panel = 'sidebar';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-3';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 6;
    $pane->locks = array();
    $pane->uuid = '4863f99f-36b8-4233-aba9-1e8314dc6c9e';
    $display->content['new-4863f99f-36b8-4233-aba9-1e8314dc6c9e'] = $pane;
    $display->panels['sidebar'][6] = 'new-4863f99f-36b8-4233-aba9-1e8314dc6c9e';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['node_view_panel_context'] = $handler;

  return $export;
}
