<?php

namespace Drupal\trading_hours\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Trading hours' block.
 *
 * @Block(
 *   id = "skygate_trading_hours_block",
 *   admin_label = @Translation("Today's hours")
 * )
 */
class TradingHoursBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config          = \Drupal::config('trading_hours.settings');
    $config_dates    = $config->get('date');
    $today           = date('Y-m-d');
    $config_weekdays = $config->get('weekday');
    $dates_data      = [];

    foreach ($config_dates as $date => $values) {
      $config_date              = date('Y-m-d', strtotime($date));
      $dates_data[$config_date] = $values['trading_hours'];
    }

    if (array_key_exists($today, $dates_data)) {
      $markup = $dates_data[$today];
    }
    else {
      $today    = date('l', strtotime($today));
      $week_day = strtolower($today);
      $markup   = $config_weekdays[$week_day]['trading_hours'];
    }

    \Drupal::service('page_cache_kill_switch')->trigger();

    return [
      '#theme'  => 'trading_hours',
      '#trading' => $markup,
      '#cache' => ['max-age' => 3600],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 3600;
  }

}
