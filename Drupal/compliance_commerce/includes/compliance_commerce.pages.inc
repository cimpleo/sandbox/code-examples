<?php
/**
 * @file
 * compliance_commerce.pages.inc
 */

function compliance_commerce_load_product_page($account = NULL) {
  global $user;
  $account  = empty($account) ? $user : $account;

  $node = compliance_structure_get_subtype('user_products');
  if (empty($node)) {
    return array();
  }

  $build = array();
  $build['node_body'] = field_view_field('node', $node, 'body', array('label' => 'hidden'));
  compliance_commerce_products_page($build);

  return $build;
}

/**
 * [compliance_user_products_page description]
 *
 * @param  [type] $user [description]
 *
 * @return [type]       [description]
 */
function compliance_commerce_products_page(&$build) {
  global $user;

  $title = str_replace('My ', '', drupal_get_title());

  // Load all products by default.
  $products = compliance_commerce_get_taxonomy_nodes('service', TRUE);
  $products['#weight'] = 100;
  if (empty($products['#items'])) {
    $plan_link = l('Plans', 'plans');
    $product_link = l('Products', 'products');
    $products = array(
      '#type' => 'markup',
      '#markup' => '<div class="empty"><p>You currently do not have any active products.</p><p>Make your first purchase by choosing from one of our ' . $plan_link . ' or ' . $product_link . '.</p></div>',
    );

    drupal_add_css('.empty { margin: 2em 0 0 0; background-color: #eee; text-align: center; padding: 2em 4em; } .empty p { color: #666; }', 'inline');
  }

  drupal_alter('compliance_commerce_products_page', $products, $build);

  // Add Products to the user profile page.
  // Only show products they have access too.
  // Alter the links to point to the actual products they have access too.
  $build['user_products'] = $products;
  $build['user_products']['#weight'] = 100;

  return $build;
}

/**
 * [compliance_commerce_user_products description]
 *
 * @param  [type] &$products [description]
 *
 * @return [type]            [description]
 */
function compliance_commerce_user_products($user, $product) {
  $page = array();

  $subtype_node = compliance_structure_get_subtype('service');
  if (!empty($subtype_node)) {
    menu_set_active_item('node/' . $subtype_node->nid);
  }

  // Load the product using entity metadata wrapper.
  $wrapper = entity_metadata_wrapper('commerce_product', $product);

  // Get the products display node.
  $nodes = compliance_commerce_get_display_nodes(array($product), 'service', 'object');
  $node = reset($nodes);

  $node_wrapper = entity_metadata_wrapper('node', $node);

  // Attach the category icon.
  $category = empty($node_wrapper->field_service_category) ? array()
    : $node_wrapper->field_service_category->value();
  $category_name = empty($category) ? NULL : $category->name;

  // Outpout the product display title and body.
  // $page['title'] = array(
  //   '#type' => 'markup',
  //   '#markup' => '<h1>' . $category_name . ' - ' . $node->title . '</h1>',
  //   '#weight' => -100,
  // );

  $page['body'] = field_view_field('node', $node, 'body', array('label' => 'hidden'));

  // Product bundle access.
  $links = empty($wrapper->field_links) ? array()
    : $wrapper->field_links->value();
  if (empty($links)) {
    return $page;
  }

  $page['product_links'] = field_view_field('commerce_product', $product, 'field_links', array('label' => 'hidden'));
  $page['product_links']['#weight'] = 100;

  return $page;
}

/**
 * [compliance_commerce_user_orders description]
 *
 * @return [type] [description]
 */
function compliance_commerce_user_orders() {
  global $user;

  $output = array(
    // 'title' => array(
    //   '#markup' => '<h1>' . format_username($user) . ' - Orders</h1>',
    // ),
    'view' => array(
      '#markup' => views_embed_view('commerce_user_orders', 'order_page', $user->uid),
    ),
  );

  return $output;
}

/**
 * [compliance_commerce_administration_page description]
 *
 * @param  [type] $user [description]
 *
 * @return [type]       [description]
 */
function compliance_commerce_administration_page($user) {
  $output = array();

  $output['new'] = array(
    '#theme' => 'item_list',
    '#title' => t('New System'),
    '#items' => array(
      l('Manage Orders', 'admin/commerce/orders'),
      l('Manage Customer Profiles', 'admin/commerce/customer-profiles'),
      l('Manage Licenses', 'admin/commerce/licenses'),
      l('Manage Coupons', 'admin/commerce/coupons'),
      l('Manage Discounts', 'admin/commerce/store/discounts'),
      l('Generate Order Report', 'admin/content/ftlf/order-report'),
      l('Download Past Reports', 'admin/content/ftlf/reports'),
    ),
  );

  $output['legacy'] = array(
    '#theme' => 'item_list',
    '#title' => t('Legacy System'),
    '#items' => array(
      l('Search All Orders', 'admin/store/orders/search'),
      l('Download All Orders', 'orders'),
      l('Download All Subscribers', 'ordered-subscriptions'),
      l('Site Documents', 'subscriber/documents'),
      l('Website Pages', 'subscriber/pages'),
      l('Search Topic / Author', 'subscriber/topics'),
      l('Compliance Risk Assessments', 'subscriber/audits/admin'),
    ),
  );

  return $output;
}
