<?php

/**
 * @file
 * Provides a license type plugin.
 */

$plugin = array(
  'title' => t('Compliance License'),
  'class' => 'ComplianceCommerceLicense',
  'weight' => 1,
);
