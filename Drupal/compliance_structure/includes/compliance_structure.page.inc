<?php
/**
 * @file
 * compliance_structure.page.inc
 */

/**
 * Landing page for Risk Areas.
 */
function compliance_structure_risk_areas_landing() {
  $page = 'RISK AREAS';
  return $page;
}

/**
 * Landing page for Plans.
 */
function compliance_structure_plans_landing() {
  $page = 'PLANS';
  return $page;
}
