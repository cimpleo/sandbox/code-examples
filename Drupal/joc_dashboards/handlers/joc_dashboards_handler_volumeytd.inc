<?php

/**
 * A handler to display dates as just the time if today, otherwise as time and date.
 *
 * @ingroup views_field_handlers
 */
class joc_dashboards_handler_volumeytd extends views_handler_field {
  function option_definition() {
    $options = parent::option_definition();
    $options['volumeytd'] = array('default' => '');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['volumeytd'] = array(
      '#type' => 'textfield',
      '#title' => t('Tokens'),
      '#description' => t('Enter two values.'),
      '#default_value' => $this->options['volumeytd'] ? $this->options['volumeytd'] : '!1;!2;!3;[field_data_set]',
    );

    parent::options_form($form, $form_state);
  }

  function query() {
    // do nothing -- to override the parent query.
  }

  function render($data) {
    // If the devel module is enabled, you may view all of the
    // data provided by fields previously added in your view.

    $tokens = explode(';', $this->options['volumeytd']);
    $vals = $this->get_render_tokens('');
    $dashboardName = $vals[$tokens[1]];
    $typeName = $vals[$tokens[2]];
    $term_name = $vals[$tokens[3]];

    $tradeLaneId = $vals[$tokens[0]];
    $dashboardId = joc_dashboards_getTermId($dashboardName, 'joc_charts_dashboard');
    $typeId = joc_dashboards_getTermId($typeName, 'joc_charts_type');
    $tid = is_numeric($term_name) ? $term_name : joc_dashboards_getTermId($term_name, 'joc_charts_data_set');

    # $currentDate = getCurrentDate(array('volume'), '', $tradeLaneId, $dashboardId, $typeId, $tid, 'DESC');
    # $yearago = strtotime(date('Y-m-d', $currentDate) . ' -1 year');
    $yeartodate = strtotime('1 Jan this year');
    $today = strtotime('now');

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'dashboard_charts')
      ->propertyCondition('status', 1)
      ->propertyCondition('created', $yeartodate, '>=')
      ->propertyCondition('created', $today, '<=')
      ->propertyOrderBy('created', 'ASC')
      ->fieldCondition('field_import_type ', 'value', array('volume'), 'IN')
      ->fieldCondition('field_trade_lane', 'tid', $tradeLaneId)
      ->fieldCondition('field_dashboard', 'tid', $dashboardId)
      ->fieldCondition('field_type', 'tid', $typeId)
      ->fieldCondition('field_data_set', 'tid', $tid);
    $result = $query->execute();

    if (isset($result['node'])) {
      $dashboard_charts_nids = array_keys($result['node']);
      $dashboard_charts_items = entity_load('node', $dashboard_charts_nids);

      $ytd = 0;
      foreach ($dashboard_charts_items as $nid) {
        $volume = $nid->field_volume['und'][0]['value'];
        $ytd = bcadd($ytd, $volume, 0);
      }

      return number_format($ytd, 0, '.', ',');
    }

    return 0;
  }
}
