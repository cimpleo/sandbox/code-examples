<?php

/**
 * A handler to display dates as just the time if today, otherwise as time and date.
 *
 * @ingroup views_field_handlers
 */
class joc_dashboards_handler_volumepriorytd extends views_handler_field {
  function option_definition() {
    $options = parent::option_definition();
    $options['volumepriorytd'] = array('default' => '');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['volumepriorytd'] = array(
      '#type' => 'textfield',
      '#title' => t('Tokens'),
      '#description' => t('Enter two values.'),
      '#default_value' => $this->options['volumepriorytd'] ? $this->options['volumepriorytd'] : '!1;!2;!3;[field_data_set];[volumeytd]',
    );

    parent::options_form($form, $form_state);
  }

  function query() {
    // do nothing -- to override the parent query.
  }

  function render($data) {
    // If the devel module is enabled, you may view all of the
    // data provided by fields previously added in your view.

    $tokens = explode(';', $this->options['volumepriorytd']);
    $vals = $this->get_render_tokens('');
    $dashboardName = $vals[$tokens[1]];
    $typeName = $vals[$tokens[2]];
    $term_name = $vals[$tokens[3]];

    $tradeLaneId = $vals[$tokens[0]];
    $dashboardId = joc_dashboards_getTermId($dashboardName, 'joc_charts_dashboard');
    $typeId = joc_dashboards_getTermId($typeName, 'joc_charts_type');
    $tid = is_numeric($term_name) ? $term_name : joc_dashboards_getTermId($term_name, 'joc_charts_data_set');
    $ytd = $vals[$tokens[4]];

    $currentDate = getCurrentDate(array('volume'), '', $tradeLaneId, $dashboardId, $typeId, $tid, 'DESC');
    $yearago = strtotime(date('Y-m-d', $currentDate) . ' -1 year');
    # $prioryear = strtotime(date('Y-m-d', $currentDate) . ' -2 years');
    # $yeartodate = strtotime('1 Jan this year');
    # $today = strtotime('now');
    $prioryeartodate = strtotime('1 Jan last year');
    $today_of_last_year = strtotime('now -1 year');

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'dashboard_charts')
      ->propertyCondition('status', 1)
      ->propertyCondition('created', $prioryeartodate, '>=')
      ->propertyCondition('created', $yearago, '<=')
      ->propertyOrderBy('created', 'ASC')
      ->fieldCondition('field_import_type ', 'value', array('volume'), 'IN')
      ->fieldCondition('field_trade_lane', 'tid', $tradeLaneId)
      ->fieldCondition('field_dashboard', 'tid', $dashboardId)
      ->fieldCondition('field_type', 'tid', $typeId)
      ->fieldCondition('field_data_set', 'tid', $tid);
    $result = $query->execute();

    if (isset($result['node'])) {
      $dashboard_charts_nids = array_keys($result['node']);
      $dashboard_charts_items = entity_load('node', $dashboard_charts_nids);

      $priorytd = 0;
      foreach ($dashboard_charts_items as $nid) {
        $volume = $nid->field_volume['und'][0]['value'];
        $priorytd = bcadd($priorytd, $volume, 0);
      }

      $needles = array('%', ',', '$');
      $ytd = str_replace($needles, '', $ytd);
      $priorytd = str_replace($needles, '', $priorytd);

      return getPercentage($ytd, $priorytd);
    }

    return 0;
  }
}
