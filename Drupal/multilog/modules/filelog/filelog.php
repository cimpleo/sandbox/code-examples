<?php

class filelog extends multilog_base {
  /**
   * @var string
   */
  private $directory = '';
  /**
   * @var int
   */
  private $standalone = 1;

  /**
   * @param  string $directory
   */
  public function setDirectory($directory) {
    $this->directory = $directory;
  }

  /**
   * @param int $standalone
   */
  public function setStandalone($standalone) {
    $this->standalone = (int) $standalone;
  }

  /**
   * @param array $event
   */
  public function writeEvent(array $event) {
    if ($this->filterExists($event['type'])) {
      $message = $this->getMessage($event);
      $log_path = $this->getPath($event['type']);
      @file_put_contents($log_path, $message, FILE_APPEND);
    }
  }

  /**
   * @param string $type
   *
   * @return string
   */
  private function getPath($type) {
    $log = '';

    foreach ($this->filters as $filter) {
      if ($type == $filter['key']) {
        $log = $type;
      }
    }

    $path = $this->directory;
    if (!is_dir($path)) {
      @mkdir($path);
    }
    if (!is_file($path . '/.htaccess')) {
      $htaccess = $path . '/.htaccess';
      @file_put_contents($htaccess, $this->getDefaultHtaccess());
      @chmod($htaccess, 0644);
    }

    $path .= '/' . $log;
    if (!is_dir($path) && !isset($dirs[$log])) {
      @mkdir($path, 0775, TRUE);
    }

    $name = ((!$this->standalone) ? uniqid($log . '-', TRUE) : $log)
      . '.log';

    return $path . '/' . $name;
  }

  /**
   * @return string
   */
  private function getDefaultHtaccess() {
    return '# Deny from all';
  }
}