<?php
$rows = array();

foreach ($filters as $key => $filter) {
  if(is_int($key)) {
    unset($filters[$key]['delete']['#title']);
    unset($filters[$key]['key']['#title']);
    unset($filters[$key]['level']['#title']);
    unset($filters[$key]['format']['#title']);
    $rows[] = array(
      drupal_render($filters[$key]['delete']),
      drupal_render($filters[$key]['key']),
      drupal_render($filters[$key]['level']),
      drupal_render($filters[$key]['format']),
    );
  }
}
unset($filters['new']['key']['#title']);
unset($filters['new']['level']['#title']);
unset($filters['new']['format']['#title']);

$rows[] = array(
  '',
  drupal_render($filters['new']['key']),
  drupal_render($filters['new']['level']),
  drupal_render($filters['new']['format']),

);

$output = theme(
  'table', array(
  'header' => array(t('Delete'), t('Type'), t('Level'), t('Format')),
  'rows'   => $rows,
)
);
$output .= drupal_render($nodes);

echo $output;
