<?php
function job_queue_ui_views_data_alter(&$data) {
  $data['job_queue']['status']['field']['handler'] = 'job_queue_ui_handler_field_status';
  $data['job_queue']['status']['filter']['handler'] = 'views_handler_filter_in_operator';
  $data['job_queue']['status']['filter']['options callback'] = 'job_queue_ui_filter_options_status';

  $data['job_queue']['created_at']['filter']['handler'] = 'job_queue_ui_handler_filter_datetime';

  $data['job_queue']['source']['filter']['handler'] = 'views_handler_filter_in_operator';
  $data['job_queue']['source']['filter']['options callback'] = 'job_queue_ui_filter_options_source';

  $data['job_queue']['handler']['filter']['handler'] = 'views_handler_filter_in_operator';
  $data['job_queue']['handler']['filter']['options callback'] = 'job_queue_ui_filter_options_handler';

  $data['job_queue']['data'] = array(
    'title' => t('Data'),
    'help' => t('Job data.'),
    'field' => array(
      'handler' => 'job_queue_ui_handler_field_data',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
}

function job_queue_ui_filter_options_handler() {
  $options = array();

  $result = db_select('job_queue', 'jq')
    ->fields('jq', array('handler'))
    ->distinct()
    ->execute();

  foreach ($result as $row) {
    $options[$row->handler] = $row->handler;
  }

  return $options;
}

function job_queue_ui_filter_options_status() {
  return array(
    JOB_QUEUE_STATUS_ACTIVE => t('Active'),
    JOB_QUEUE_STATUS_SUCCESS => t('Success'),
    JOB_QUEUE_STATUS_FAIL => t('Fail'),
    JOB_QUEUE_STATUS_FATAL => t('Fatal'),
    JOB_QUEUE_STATUS_DISABLED => t('Disabled'),
  );
}

function job_queue_ui_filter_options_source() {
  $options = array();

  $result = db_select('job_queue', 'jq')
    ->fields('jq', array('source'))
    ->distinct()
    ->execute();

  foreach ($result as $row) {
    $options[$row->source] = $row->source;
  }

  return $options;
}

function job_queue_ui_views_default_views() {
  $path = realpath(drupal_get_path('module', 'job_queue_ui') . '/job_queue_ui.view');
  require_once $path;
  $views[$view->name] = $view;

  return $views;
}