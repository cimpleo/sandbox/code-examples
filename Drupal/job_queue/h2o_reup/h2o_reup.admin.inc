<?php

function h2o_reup_pins_pull_delete($pin) {
  db_delete('h2o_reup_pins_pool')
          ->condition('pin', $pin)
          ->execute()
  ;
  drupal_set_message('Pin number deleted');
  drupal_goto();
}

function h2o_reup_plans_alias($form, $form_state) {
  $tid = variable_get('h2o_term', 9);
  $nids = taxonomy_select_nodes($tid, FALSE);
  $nodes = node_load_multiple($nids, array('status' => 1));

  $plan_aliases = variable_get('h2o_reup_plans_alias', array());
  $form['h2o_reup_plans_alias'] = array(
      '#tree' => TRUE
  );
  $form['h2o_reup_auto_change_plan'] = array(
      '#type' => 'checkbox',
      '#title' => 'Enable auto change plan',
      '#default_value' => variable_get('h2o_reup_auto_change_plan', 0)
  );
  foreach ($nodes as $node) {
    if ($node->type == 'product') {
      $form['h2o_reup_plans_alias'][$node->nid] = array(
          '#tree' => TRUE,
          'plan_name' => array(
              '#type' => 'item',
              '#markup' => "<b>{$node->title}</b>"
          ),
          'alias' => array(
              '#type' => 'textfield',
              '#default_value' => isset($plan_aliases[$node->nid]) ? $plan_aliases[$node->nid]['alias'] : ''
          ),
      );
    }
  }

  return system_settings_form($form);
}

function h2o_reup_autocancel($form, $form_state) {


  $form['h2o_autocancel_enable'] = array(
      '#type' => 'checkbox',
      '#title' => 'Enable auto cancel',
      '#default_value' => variable_get('h2o_autocancel_enable', 0)
  );

  $form['h2o_autocancel_check_phone'] = array(
      '#type' => 'textarea',
      '#title' => 'Failed check phone',
      '#default_value' => variable_get('h2o_autocancel_check_phone', 'Your order canceled')
  );

  $form['h2o_autocancel_change_plan'] = array(
      '#type' => 'textarea',
      '#title' => 'Failed change plan',
      '#default_value' => variable_get('h2o_autocancel_change_plan', 'Your order canceled')
  );

  $form['h2o_autocancel_reup'] = array(
      '#type' => 'textarea',
      '#title' => 'Failed reup',
      '#default_value' => variable_get('h2o_autocancel_reup', 'Your order canceled')
  );

  return system_settings_form($form);
}
