<?php
function tms_menu() {
  $items = array();

  $items['admin/config/system/tms'] = array(
    'type' => MENU_NORMAL_ITEM,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tms_settings_form'),
    'access arguments' => array('administer tms'),
    'title' => 'TMS settings',
    'file' => 'tms.admin.inc'
  );

  $items['admin/config/system/tms/errors_settings'] = array(
    'type' => MENU_NORMAL_ITEM,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tms_error_settings_form'),
    'access arguments' => array('administer tms'),
    'title' => 'TMS Error code settings',
    'file' => 'tms.admin.inc'
  );

  $items['admin/config/system/tms/errors_settings/delete/%'] = array(
    'type' => MENU_CALLBACK,
    'page callback' => 'tms_error_settings_delete',
    'page arguments' => array(6),
    'access arguments' => array('administer tms'),
    'file' => 'tms.admin.inc'
  );

  return $items;
}

function tms_permission() {
  return array(
    'administer tms' => array(
      'title' => 'administer tms',
      'description' => 'administer tms'
    ),
    'tms orders action' => array(
      'title' => 'tms orders action',
      'description' => 'tms orders action'
    ),
  );
}

function tms_theme() {
  return array(
    'tms_error_settings_form' => array(
      'render element' => 'form',
      'template' => 'templates/tms_error_settings_form'
    )
  );
}

function tms_doTransactionUp($order) {
  $process_id = uniqid();
  watchdog('tms', "Start doTransactionUp ({$process_id})\n OrderId: {$order->order_id}", array(), WATCHDOG_INFO);
  $processPossible = _tms_orderProcessPossible($order);
  watchdog('tms', "doTransactionUp ({$process_id})\n OrderId: {$order->order_id} \n Possible process: {$processPossible}" , array(), WATCHDOG_DEBUG);
  $products_key = array_keys($order->products);
  $product_node = node_load($order->products[$products_key[0]]->nid);
  $fees = !empty($product_node->field_fee) ? $product_node->field_fee['und'][0]['value'] : 0;
  $product_id = !empty($product_node->field_rtr_product_code) ? $product_node->field_rtr_product_code['und'][0]['value'] : '';
  watchdog('tms', "doTransactionUp ({$process_id})\n OrderId: {$order->order_id} \n Product Id: {$product_id}" , array(), WATCHDOG_DEBUG);
  if (is_bool($processPossible) && !empty($product_id)) {
    require_once dirname(__FILE__) . '/include/tms.php';
    $tms = new \tms\tms();
    $tms->setConnectionTimeout(variable_get('tms_connection_timeout', 30));
    $tms->setUser(variable_get('tms_username', ''));
    $tms->setPass(variable_get('tms_password', ''));
    $tms->setTrace(1);
    $tms->setTestMode(variable_get('tms_test_mode', 0));
    $terminal_id = variable_get('tms_terminal_id', '');
    if ($tms->login() === FALSE) {
      $msg = "TMS API Login error: ({$process_id}):" . $tms->getLastErrorCode() . ': ' . $tms->getLastError();
      $msg .= "\n Request: " . var_export($tms->getLastRequest(), TRUE);
      $msg .= "\n Response: " .var_export($tms->getLastResponse(), TRUE);
      watchdog('tms', $msg , array(), WATCHDOG_ERROR);
      uc_order_comment_save($order->order_id, 1, 'TMS API Login error: ' . $tms->getLastErrorCode() . ': ' . $tms->getLastError());
      uc_order_update_status($order->order_id, 'completed');
      return FALSE;
    }

    if (!empty($terminal_id)) {
      $tms->setTerminalId($terminal_id);
    }

    $request_data = array(
      'amount' => $order->order_total,
      'productId' => $product_id,
      'localTime' => $order->created,
      'orderId' => $order->order_id,
      'phone' => $order->phone,
      'fees' => $fees
    );
    $response = $tms->doTransactionUp($request_data);
    $msg = "TMS API process: ({$process_id}):" . $tms->getLastErrorCode() . ': ' . $tms->getLastError();
    $msg .= "\n Request: " . var_export($tms->getLastRequest(), TRUE);
    $msg .= "\n Response: " .var_export($tms->getLastResponse(), TRUE);
    watchdog('tms', $msg , array(), WATCHDOG_DEBUG);

    $reup_codes = explode(',', variable_get('tms_reup_codes', ''));
    $tms_error_config = variable_get('tms_error_config', array());
    if ($response === FALSE) {
      $msg = "TMS API error: ({$process_id}):" . $tms->getLastErrorCode() . ': ' . $tms->getLastError();
      $msg .= "\n Request: " . var_export($tms->getLastRequest(), TRUE);
      $msg .= "\n Response: " .var_export($tms->getLastResponse(), TRUE);
      watchdog('tms', $msg , array(), WATCHDOG_ERROR);
      uc_order_comment_save($order->order_id, 1, 'TMS API DoTransactionUP error: ' . $tms->getLastErrorCode() . ': ' . $tms->getLastError());
      uc_order_comment_save($order->order_id, 1, 'TMS API Transaction canceled');
      $run = TRUE;
      $i = 0;
      while ($i < 5 && $run) {
        $result = $tms->cancel($request_data);
        if ($result === FALSE) {
          $run = !($result->DoTransactionUPResult->Result === 0);
        }
        else {
          $i++;
        }
      }
      uc_order_update_status($order->order_id, 'completed');
      return FALSE;
    }

    if ($response->DoTransactionUPResult->Result === 0) {
      $price = strip_tags(theme('uc_price', array('price' => $order->order_total)));
      $message = 'We have successfully added ' . $price . ' to your wireless number. Thank you for using SimpleMobileBillPay.com.';
      uc_order_comment_save($order->order_id, 1, 'TMS API DoTransactionUP success: ' . $response->DoTransactionUPResult->Result . ': ' . $response->DoTransactionUPResult->ResultText);
      uc_order_update_status($order->order_id, 'completed_processed');
      $number = $order->phone;
      job_queue_add('twilio_sms',
        array(
          'phone' => $number,
          'message' => $message
        ),
        array(
          'source' => 'tms_DoTransactionUp'
        )
      );
    }
    elseif (in_array($response->DoTransactionUPResult->Result, $reup_codes)) {
      uc_order_comment_save($order->order_id, 1, 'TMS API DoTransactionUP error: ' . $response->DoTransactionUPResult->Result . ': ' . $response->DoTransactionUPResult->ResultText);
      tms_create_reprocess_job($order);
      uc_order_update_status($order->order_id, 'completed');
    }
    elseif (variable_get('tms_enable_canceled_orders', 0) && array_key_exists($response->DoTransactionUPResult->Result, $tms_error_config)) {
      uc_order_comment_save($order->order_id, 1, 'TMS API DoTransactionUP error: ' . $response->DoTransactionUPResult->Result . ': ' . $response->DoTransactionUPResult->ResultText);
      uc_order_update_status($order->order_id, 'canceled');
      tms_add_order_comment($order->order_id, $tms_error_config[$response->DoTransactionUPResult->Result], 'canceled');
      return TRUE;
    }
    else {
      uc_order_comment_save($order->order_id, 1, 'TMS API DoTransactionUP error: ' . $response->DoTransactionUPResult->Result . ': ' . $response->DoTransactionUPResult->ResultText);
//      if (variable_get('tms_enable_canceled_orders', 0)) {
//        uc_order_update_status($order->order_id, 'canceled');
//        tms_add_order_comment($order->order_id, variable_get('tms_canceled_comment', ''), 'canceled');
//      }
//      else {
        uc_order_update_status($order->order_id, 'completed');
//      }
    }
  }
  else {
    uc_order_comment_save($order->order_id, 1, 'Blocked by Fraud Filters: ' . $processPossible);
    uc_order_update_status($order->order_id, 'completed');
  }
  watchdog('tms', "Finish doTransactionUp ({$process_id})", array(), WATCHDOG_INFO);
  return TRUE;
}

function _tms_orderProcessPossible($order) {
  $process = TRUE;

  if (tms_order_is_processing($order->order_id)) {
    $process = 'Tms order already processed';
  }
  else {
    db_insert('tms_order_processing')
      ->fields(array(
        'order_id' => $order->order_id
      ))
      ->execute();
  }

  if (is_bool($process) && module_exists('order_checker')) {
    $ip = module_exists('order_ip_log') ? order_ip_log_get_ip_by_order($order->order_id) : '';
    if (order_checker_is_fraud($order->phone)) {
      uc_order_comment_save($order->order_id, 1, 'Phone number is fraud', 'admin', $order->order_status);
      $process = 'Phone number is fraud';
    }
    elseif (order_checker_ip_is_fraud($ip)) {
      uc_order_comment_save($order->order_id, 1, 'IP is banned', 'admin', $order->order_status);
      $process = 'IP is banned';
    }
    elseif (order_checker_exist_full_successes_orders($order->phone)) {
      $process = 'Exist full successes orders';
    }
    else {
      $process = order_checker_check_zone_code($order)? TRUE: 'Area Code';
    }
  }

  return $process;
}

function tms_order_is_processing($order_id) {
  $count = db_select('tms_order_processing', 'tp')
    ->condition('order_id', $order_id)
    ->countQuery()
    ->execute()
    ->fetchField();

  return ($count > 0);
}

function tms_add_order_comment($order_id, $message, $status) {
  uc_order_comment_save($order_id, 1, $message, 'order', $status, TRUE);
  $order = uc_order_load($order_id);
  rules_invoke_event('uc_order_status_email_update', $order);
}

function tms_get_pins($order, $primary = TRUE) {
  $process_id = uniqid();
  watchdog('tms', "Start getPins ({$process_id})\n OrderId: {$order->order_id} \n Primary: {$primary}", array(), WATCHDOG_INFO);

  $return = array(
    'error' => -1,
    'text' => '',
    'pin' => '',
    'process_possible' => '',
  );
  if ($primary) {
    $processPossible = _tms_orderProcessPossible($order);
  }
  else {
    $processPossible = TRUE;
  }
  watchdog('tms', "getPins ({$process_id})\n OrderId: {$order->order_id} \n Possible process: {$processPossible}" , array(), WATCHDOG_DEBUG);
  $products_key = array_keys($order->products);
  $product_node = node_load($order->products[$products_key[0]]->nid);
  $fees = !empty($product_node->field_fee) ? $product_node->field_fee['und'][0]['value'] : 0;
  $product_id = !empty($product_node->field_pin_product_code) ? $product_node->field_pin_product_code['und'][0]['value'] : '';
  watchdog('tms', "getPins ({$process_id})\n OrderId: {$order->order_id} \n Product Id: {$product_id}" , array(), WATCHDOG_DEBUG);
  if (is_bool($processPossible) && !empty($product_id)) {
    require_once dirname(__FILE__) . '/include/tms.php';
    $tms = new \tms\tms();
    $tms->setConnectionTimeout(variable_get('tms_connection_timeout', 30));
    $tms->setUser(variable_get('tms_username', ''));
    $tms->setPass(variable_get('tms_password', ''));
    $tms->setTrace(1);
    $tms->setTestMode(variable_get('tms_test_mode', 0));
    $terminal_id = variable_get('tms_terminal_id', '');

    if ($tms->login() === FALSE) {
      $msg = "TMS API Login error: ({$process_id}):" . $tms->getLastErrorCode() . ': ' . $tms->getLastError();
      $msg .= "\n Request: " . var_export($tms->getLastRequest(), TRUE);
      $msg .= "\n Response: " .var_export($tms->getLastResponse(), TRUE);
      watchdog('tms', $msg , array(), WATCHDOG_ERROR);
      uc_order_comment_save($order->order_id, 1, 'TMS API Login error: ' . $tms->getLastErrorCode() . ': ' . $tms->getLastError());
      uc_order_update_status($order->order_id, 'completed');
      return FALSE;
    }

    if (!empty($terminal_id)) {
      $tms->setTerminalId($terminal_id);
    }

    $request_data = array(
      'amount' => $order->order_total,
      'productId' => $product_id,
      'localTime' => $order->created,
      'orderId' => $order->order_id,
      'phone' => $order->phone,
      'fees' => $fees
    );
    $response = $tms->getPin($request_data);
    $msg = "TMS API process: ({$process_id}):" . $tms->getLastErrorCode() . ': ' . $tms->getLastError();
    $msg .= "\n Request: " . var_export($tms->getLastRequest(), TRUE);
    $msg .= "\n Response: " .var_export($tms->getLastResponse(), TRUE);
    watchdog('tms', $msg , array(), WATCHDOG_DEBUG);

    if ($response === FALSE) {
      $msg = "TMS API process: ({$process_id}):" . $tms->getLastErrorCode() . ': ' . $tms->getLastError();
      $msg .= "\n Request: " . var_export($tms->getLastRequest(), TRUE);
      $msg .= "\n Response: " .var_export($tms->getLastResponse(), TRUE);
      watchdog('tms', $msg , array(), WATCHDOG_ERROR);

      $return['text'] = 'TMS API DoTransactionUP error: ' . $tms->getLastErrorCode() . ': ' . $tms->getLastError();
      $run = TRUE;
      $i = 0;
      while ($i < 5 && $run) {
        $result = $tms->cancel($request_data);
        if ($result === FALSE) {
          $run = !($result->DoTransactionUPResult->Result === 0);
        }
        else {
          $i++;
        }
      }
    }
    else {
      if ($response->DoTransactionUPResult->Result === 0) {
        $return['error'] = $response->DoTransactionUPResult->Result;
        $return['text'] = $response->DoTransactionUPResult->ResultText;
        if (is_object($response->DoTransactionUPResult->PinCredentials)) {
          $return['text'] .= '<br />Pin number: ' . $response->DoTransactionUPResult->PinCredentials->PIN->Text;
          $return['pin'] = $response->DoTransactionUPResult->PinCredentials->PIN->Text;
          $return['text'] .= '<br />Serial number: ' . $response->DoTransactionUPResult->PinCredentials->Serial;
        }
      }
      else {
        $return['error'] = $response->DoTransactionUPResult->Result;
        $return['text'] = $response->DoTransactionUPResult->ResultText;
      }
    }
  }
  else {
    $return['process_possible'] = $processPossible;
  }
  watchdog('tms', "Finish getPins ({$process_id})", array(), WATCHDOG_INFO);
  return $return;
}

function tms_create_reprocess_job($order) {
  $product_keys = array_keys($order->products);
  $product_node = node_load($order->products[$product_keys[0]]->nid);
  $carrier_term = taxonomy_term_load($product_node->field_carrier['und'][0]['tid']);
  if (!empty($carrier_term->field_error_callback) && function_exists($carrier_term->field_error_callback['und'][0]['value'])) {
    $funciton = $carrier_term->field_error_callback['und'][0]['value'];
    $funciton($order);
  }
}

function tms_process_orders($order) {
  $product_keys = array_keys($order->products);
  $product_node = node_load($order->products[$product_keys[0]]->nid);
  if (!empty($product_node->field_pinles) && $product_node->field_pinles['und'][0]['value']) {
    $carrier_term = taxonomy_term_load($product_node->field_carrier['und'][0]['tid']);
    if (!empty($carrier_term->field_pin_callback) && function_exists($carrier_term->field_pin_callback['und'][0]['value'])) {
      $function = $carrier_term->field_pin_callback['und'][0]['value'];
      $function($order);
    }
    else {
      $response = tms_get_pins($order);
      $tms_error_config = variable_get('tms_error_config', array());
      if ($response['error'] === 0) {
        uc_order_comment_save($order->order_id, 1, $response['text']);
        $message = 'We have successfully processed your order for ' . theme('uc_price', array('price' => $order->order_total)) .
          '. Your refill pin is: ' . $response['pin'] .
          '. Thank you for using pageplusinstantrefill.com.';
        job_queue_add('twilio_sms',
          array(
            'phone' => $order->phone,
            'message' => $message
          ),
          array(
            'source' => 'tms_process_orders'
          )
        );
        $order->data['pin_number'] = $response['pin'];
        uc_order_save($order);
        uc_order_update_status($order->order_id, 'completed_processed');
      }
      elseif (variable_get('tms_enable_canceled_orders', 0) && array_key_exists($response['error'], $tms_error_config)) {
        uc_order_comment_save($order->order_id, 1, 'TMS API DoTransactionUP error: ' . $response['error'] . ': ' . $response['text']);
        uc_order_update_status($order->order_id, 'canceled');
        tms_add_order_comment($order->order_id, $tms_error_config[$response['error']], 'canceled');
        return TRUE;
      }
      elseif (!empty($response['process_possible'])) {
        uc_order_comment_save($order->order_id, 1, 'Blocked by Fraud Filters: ' . $response['process_possible']);
        uc_order_update_status($order->order_id, 'completed');
      }
      else {
        uc_order_comment_save($order->order_id, 1, 'TMS API DoTransactionUP error: ' . $response['error'] . ': ' . $response['text']);
//        if (variable_get('tms_enable_canceled_orders', 0)) {
//          uc_order_update_status($order->order_id, 'canceled');
//          tms_add_order_comment($order->order_id, variable_get('tms_canceled_comment', ''), 'canceled');
//        }
//        else {
          uc_order_update_status($order->order_id, 'completed');
//        }
      }
    }
  }
  else {
    $job_id = job_queue_add('tms_queue_process', array('order_id' => $order->order_id), array('source' => 'condition action'));
    uc_order_comment_save($order->order_id, 1, "Created job( #{$job_id}) for api processing");
  }
}
