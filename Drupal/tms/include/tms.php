<?php
namespace tms;

class tms {
    const PRODUCTION_WSDL = 'epayTMS.wsdl';
    const TEST_WSDL = 'epayTMS_test.wsdl';
    const APP_NAME = 'TMS';

    /**
     * @var bool
     */
    private $testMode = FALSE;
    /**
     * @var string
     */
    private $user = '';
    /**
     * @var string
     */
    private $pass = '';
    /**
     * @var int
     */
    private $connectionTimeout = 30;
    /**
     * @var string
     */
    private $lastError = '';
  private $lastErrorCode = '';
    /**
     * @var int
     */
    private $trace = 0;
    /**
     * @var null|object
     */
    private $lastRequest = null;
    /**
     * @var null|object
     */
    private $lastResponse = null;

    /**
     * @var string
     */
    private $terminalId = '';

  /**
   * @var string
   */
    private $authenticationId = '';


    /**
     * @param string $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @param string $pass
     */
    public function setPass($pass)
    {
        $this->pass = $pass;
    }

    /**
     * @param int $timeout
     */
    public function setConnectionTimeout($timeout)
    {
        $this->connectionTimeout = $timeout;
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return $this->lastError;
    }

    /**
     * @return string
     */
    public function getLastErrorCode()
    {
        return $this->lastErrorCode;
    }

    /**
     * @param int $trace
     */
    public function setTrace($trace)
    {
        $this->trace = $trace;
    }

    /**
     * @param string $action
     * @param array $params
     * @return bool|mixed
     */
    private function sendRequest($action, $params)
    {
        if (function_exists('xdebug_disable')){
            xdebug_disable();
        }

        $response = false;
        try {
            $config = array(
                'conection_timeout' => $this->connectionTimeout,
                'trace' => $this->trace,
            );
            $wsdl_file = $this->testMode ? self::TEST_WSDL : self::PRODUCTION_WSDL;
            $wsdl_path = dirname(__FILE__) . "/{$wsdl_file}";
            $soap = @new \SoapClient($wsdl_path, $config);
            $response = $soap->__soapCall($action, $params);
            $this->lastRequest = $soap->__getLastRequest();
            $this->lastResponse = $soap->__getLastResponse();
        }
        catch (\SoapFault $ex) {
            if (is_object($soap)) {
                $this->lastRequest = $soap->__getLastRequest();
                $this->lastResponse = $soap->__getLastResponse();
            }

            $this->lastError = $ex->getMessage();
            $this->lastErrorCode = $ex->getCode();
        }

        if (function_exists('xdebug_enable')){
            xdebug_enable();
        }

        return $response;
    }

    /**
     * @return bool|mixed
     */
    public function login()
    {
        $params =  array(
            'param' => array(
                'username' => $this->user,
                'password' => $this->pass,
                'appName' => self::APP_NAME
            )
        );

        $response = $this->sendRequest('Login', $params);

        if (is_object($response)) {
            $this->terminalId = $response->LoginResult->AppProfile->TID;
            $this->authenticationId = $response->LoginResult->AuthenticationId;
        }

        return $response;
    }

    /**
     * @param bool $enable
     */
    public function setTestMode($enable)
    {
        $this->testMode = $enable;
    }

    /**
     * @param string $terminalId
     */
    public function setTerminalId($terminalId)
    {
        $this->terminalId = $terminalId;
    }

    /**
     * @param array $data
     * @return bool|mixed
     */
    public function doTransactionUp($data)
    {
        $params = array(
            'param' => array(
                'request' => array(
                    'Amount' => isset($data['amount']) ? $data['amount'] * 100 : 0,
                    'Authorization' => array(),
                    'CustomData' => array(
                        'Items' => array(
                            'CustomDataItem' => array(
                                'Values' => array(
                                    'CustomDataItemValue' => array(
                                        'Key' => 'PROMPT1',
                                        'Value' => isset($data['phone']) ? $data['phone'] : '',
                                    ),
                                ),
                            ),
                        ),
                    ),
                    'LocalDateTime' => isset($data['LocalTime']) ? date("Y-m-d H:i:s", $data['LocalTime']) : date("Y-m-d H:i:s"),
                    'Mode' => 'DIRECT',
                    'ProductID' => isset($data['productId'])? $data['productId'] : '',
                    'RequestType' => 'SALE',
                    'TerminalID' => $this->terminalId,
                    'TransactionID' => isset($data['orderId']) ? $data['orderId'] : ''
                ),
            ),
        );

        if (!empty($this->authenticationId)) {
            $params['param']['authId'] = $this->authenticationId;
        }
        else {
            $params['param']['request']['Authorization']['Username'] = $this->user;
            $params['param']['request']['Authorization']['Password'] = $this->pass;
        }

        if (isset($data['fees']) && $data['fees'] > 0){
            $params['param']['request']['ServiceFee'] = $data['fees'] * 100;
            $amount = $data['amount'] - $data['fees'];
            $params['param']['request']['Amount'] = $amount * 100;

        }

        $response = $this->sendRequest('DoTransactionUp', $params);

        return $response;
    }

    /**
     * @param array $data
     * @return bool|mixed
     */
    public function getPin($data)
    {
        $params = array(
            'param' => array(
                'request' => array(
                    'Amount' => isset($data['amount']) ? $data['amount'] * 100 : 0,
                    'Authorization' => array(),
                    'LocalDateTime' => isset($data['LocalTime']) ? date("Y-m-d H:i:s", $data['LocalTime']) : date("Y-m-d H:i:s"),
                    'Mode' => 'DIRECT',
                    'ProductID' => isset($data['productId'])? $data['productId'] : '',
                    'RequestType' => 'SALE',
                    'TerminalID' => $this->terminalId,
                    'TransactionID' => isset($data['orderId']) ? $data['orderId'] : ''
                ),
            ),
        );

        if (!empty($this->authenticationId)) {
            $params['param']['authId'] = $this->authenticationId;
        }
        else {
            $params['param']['request']['Authorization']['Username'] = $this->user;
            $params['param']['request']['Authorization']['Password'] = $this->pass;
        }

        if (isset($data['fees']) && $data['fees'] > 0){
            $params['param']['request']['ServiceFee'] = $data['fees'] * 100;
            $amount = $data['amount'] - $data['fees'];
            $params['param']['request']['Amount'] = $amount * 100;

        }

        $response = $this->sendRequest('DoTransactionUp', $params);

        return $response;
    }

    /**
     * @param array $data
     * @return bool|mixed
     */
    public function refund($data)
    {
        $params = array(
            'param' => array(
                'request' => array(
                    'Amount' => isset($data['amount']) ? $data['amount'] * 100 : 0,
                    'Authorization' => array(),
                    'CustomData' => array(
                        'Items' => array(
                            'CustomDataItem' => array(
                                'Values' => array(
                                    'CustomDataItemValue' => array(
                                        'Key' => 'PROMPT1',
                                        'Value' => isset($data['phone']) ? $data['phone'] : '',
                                    ),
                                ),
                            ),
                        ),
                    ),
                    'LocalDateTime' => isset($data['LocalTime']) ? date("Y-m-d H:i:s", $data['LocalTime']) : date("Y-m-d H:i:s"),
                    'Mode' => 'DIRECT',
                    'ProductID' => isset($data['productId'])? $data['productId'] : '',
                    'RequestType' => 'Refund',
                    'TerminalID' => $this->terminalId,
                    'TransactionID' => isset($data['orderId']) ? $data['orderId'] : ''
                ),
            ),
        );

        if (!empty($this->authenticationId)) {
            $params['param']['authId'] = $this->authenticationId;
        }
        else {
            $params['param']['request']['Authorization']['Username'] = $this->user;
            $params['param']['request']['Authorization']['Password'] = $this->pass;
        }

        if (isset($data['fees']) && $data['fees'] > 0){
            $params['param']['request']['ServiceFee'] = $data['fees'] * 100;
            $amount = $data['amount'] - $data['fees'];
            $params['param']['request']['Amount'] = $amount * 100;
        }

        $response = $this->sendRequest('DoTransactionUp', $params);

        return $response;
    }
    /**
     * @param array $data
     * @return bool|mixed
     */
    public function cancel($data)
    {
        $params = array(
            'param' => array(
                'request' => array(
                    'Amount' => isset($data['amount']) ? $data['amount'] * 100 : 0,
                    'Authorization' => array(),
                    'CustomData' => array(
                        'Items' => array(
                            'CustomDataItem' => array(
                                'Values' => array(
                                    'CustomDataItemValue' => array(
                                        'Key' => 'PROMPT1',
                                        'Value' => isset($data['phone']) ? $data['phone'] : '',
                                    ),
                                ),
                            ),
                        ),
                    ),
                    'LocalDateTime' => isset($data['LocalTime']) ? date("Y-m-d H:i:s", $data['LocalTime']) : date("Y-m-d H:i:s"),
                    'Mode' => 'DIRECT',
                    'ProductID' => isset($data['productId'])? $data['productId'] : '',
                    'RequestType' => 'Cancel',
                    'TerminalID' => $this->terminalId,
                    'TransactionID' => isset($data['orderId']) ? $data['orderId'] : ''
                ),
            ),
        );

        if (!empty($this->authenticationId)) {
            $params['param']['authId'] = $this->authenticationId;
        }
        else {
            $params['param']['request']['Authorization']['Username'] = $this->user;
            $params['param']['request']['Authorization']['Password'] = $this->pass;
        }

        if (isset($data['fees']) && $data['fees'] > 0){
            $params['param']['request']['ServiceFee'] = $data['fees'] * 100;
            $amount = $data['amount'] - $data['fees'];
            $params['param']['request']['Amount'] = $amount * 100;
        }

        $response = $this->sendRequest('DoTransactionUp', $params);

        return $response;
    }

    public function getLastRequest()
    {
        return $this->lastRequest;
    }

    public function getLastResponse()
    {
        return $this->lastResponse;
    }
} 