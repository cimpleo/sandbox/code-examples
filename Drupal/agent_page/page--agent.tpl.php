<?php $images = drupal_get_path('theme', 'coldwellbanker'); 
$agent_id = arg(1);?>
<header class="clearfix">
    <div class="container">
        <div class="logo" id="logo">
            <a href="<?php print $front_page ?>">
                <?php if ($logo): ?>
                    <img src="<?php print $logo ?>"  id="logo" />
                <?php endif; ?>            
            </a>
        </div>
        <div class="navAbove">
            <div class="feedback">
                <div class="mortgageLink" id="mortgageLink">
                    <a target="_blank" href="https://www.coldwellbankermortgage.com/home/landscape?jpid=SSLoanStart&amp;cid=81408">Coldwell Banker Mortgage 888.308.6558</a>
                </div>
            </div>
            <div id="socialWrapper" class="socialWrapper">

                <div class="socialIcon pinterest">
                    <a target="_blank" href="http://www.pinterest.com/coldwellbanker">&nbsp;</a>
                </div>
                <div class="socialIcon wordpress">
                    <a target="_blank" href="http://blog.coldwellbanker.com/">&nbsp;</a>
                </div>
                <div class="socialIcon youtube">
                    <a target="_blank" href="http://www.youtube.com/coldwellbanker">&nbsp;</a>
                </div>
                <div class="socialIcon twitter">
                    <a target="_blank" href="http://twitter.com/coldwellbanker">&nbsp;</a>
                </div>
                <div class="socialIcon facebook">
                    <a target="_blank" href="http://www.facebook.com/coldwellbanker">&nbsp;</a>
                </div>
            </div>
        </div>

        <?php
        $menu = menu_tree('main-menu');
        echo render($menu);
        ?>
    </div>
</header>
<?php if ($messages): ?>
    <div id="messages"><div class="section clearfix">
            <?php print $messages; ?>
        </div></div> <!-- /.section, /#messages -->
<?php endif; ?>
<div class="page">
    <div class="page-agent-detail">
        <?php print render($page['header']); ?>          
        <div id="tabs">
            <ul>
                <li><div class="left"></div><a href="#tabs-1"><?php print t('AGENT'); ?></a><div class="right"></div></li>
                <li><div class="left"></div><a href="#tabs-2"><?php print t('OFFICE'); ?></a><div class="right"></div></li>  
                <li class="properties"><div class="left"></div><a href="#tabs-3"><?php print t('PROPERTIES'); ?></a><div class="right"></div></li>
            </ul>
            <div id="tabs-1"><div class="agentDetailContent">
                    <div class="details-container">
                        <h2><span class="name"><?php
        if ($variables['agent']) {
            print $variables['agent']['ip_usr_firstname'] . ' '
                    . $variables['agent']['ip_usr_lastname'];
        }
        ?>
                            </span></h2>
                        <div class="buero"><span>
                                <?php
                                if ($variables['office']) {
                                    print $variables['office']['buero'];
                                }
                                ?>
                            </span></div>
                        <div class="adress">
                            <?php
                            if ($variables['office']) {
                                print $variables['office']['address_city'] .
                                        ', ' . $variables['office']['address_street'] .
                                        ', ' . $variables['office']['address_postalCode'];
                            }
                            ?>
                        </div>
                        <div class="contact">
                            <?php if (!empty($variables['office']['phone'])): ?>
                                <label><?php print t('Office:'); ?></label>
                                <?php
                                print $variables['office']['phone'];
                                ?>
                            <?php endif; ?>
                            <?php if (!empty($variables['agent']['ip_usr_mobilePhone'])): ?>
                                <label><?php print t('Mobile:'); ?></label>
                                <?php
                                print $variables['agent']['ip_usr_mobilePhone'];
                                ?>
                            <?php endif; ?>
                            <?php if (!empty($variables['office']['fax'])): ?>
                                <label><?php print t('Fax:'); ?></label>
                                <?php
                                print $variables['office']['fax'];
                                ?>
                            <?php endif; ?>         
                        </div>
                        <div class="links">

                            <span class="properties"><?php print t('My Properties '); ?></span>
                            <span class="office"><?php print t('My Office '); ?></span> 
                        </div>
                    </div>
                    <div class="agent-content">
                        <div class="video">
                        </div>

                        <div class="contact-column">
                            <h2><?php print t('Contact'); ?> <span class="name"><?php
                            if ($variables['agent']) {
                                print $variables['agent']['ip_usr_firstname'] . ' '
                                        . $variables['agent']['ip_usr_lastname'];
                            }
                            ?>
                                </span></h2>
                            <div class="detailTitleDivider"> </div>
                            <div class="info">
                                <div class="photo"></div>
                                <div class="contact-info">
                                    <div class="office"><label><?php print t('Office:'); ?>
                                        </label>
                                        <?php
                                        if ($variables['office']) {
                                            print $variables['office']['buero'];
                                        }
                                        ?></div>
                                    <div class="adress"><label><?php print t('Adress:'); ?></label> <?php
                                        if ($variables['office']) {
                                            print $variables['office']['address_street']
                                                    . $variables['office']['address_postalCode']
                                                    . $variables['office']['address_city'];
                                        }
                                        ?>
                                    </div>
                                    <div class="phone"><label><?php print t('Phone:'); ?> </label>
                                        <?php
                                        if ($variables['office']) {
                                            print $variables['office']['phone'];
                                        }
                                        ?></div>
                                    <div class="fax">
                                        <?php if (!empty($variables['office']['fax'])): ?>
                                            <label><?php print t('Fax:'); ?></label>
                                            <?php
                                            print $variables['office']['fax'];
                                            ?>
                                        <?php endif; ?> 
                                    </div>
                                    <div class="mobile">
                                        <?php if (!empty($variables['agent']['ip_usr_mobilePhone'])): ?>
                                            <label><?php print t('Mobile:'); ?></label>
                                            <?php
                                            print $variables['agent']['ip_usr_mobilePhone'];
                                            ?>
                                        <?php endif; ?>
                                    </div>
                                    <div class="email"><label><?php print t('Email:'); ?></label>
                                        <?php
                                        if ($variables['agent']) {
                                            print $variables['agent']['ip_usr_email'];
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $form = module_invoke('webform', 'block_view', 'client-block-27');
                            print $form['content'];
                            ?>                          

                        </div>    
                    </div>
                    <div class="detail-sub-container">                          
                        <div class="properties"><?php print t('Properties'); ?></div>
                        <div class="share"><a href="#"><label><?php print t('Share'); ?></label></a></div>
                    </div>
                    <div class="description-agent">
                        <div class="detail-title">
                            <?php
                            if ($variables['agent']) {
                                print t('About') . ' ' . $variables['agent']['ip_usr_firstname'] . ' '
                                        . $variables['agent']['ip_usr_lastname'];
                            }
                            ?>
                        </div>
                        <p><?php
//                                if ($variables['agent']) {
//                                    print $variables['agent']['ip_usr_description'];
//                                }
                            ?>
                            Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum
                        </p>
                    </div>
                </div>              

            </div>

            <div id="tabs-2"><div class="agentDetailContent">
                    <div class="details-container">
                        <div class="min-info">
                            <div class="photo">
                                </div>
                                <h2><span class="name"><?php
                            if ($variables['agent']) {
                                print $variables['agent']['ip_usr_firstname'] . ' '
                                        . $variables['agent']['ip_usr_lastname'];
                            }
                            ?>
                                    </span></h2>
                            <div class="contact"><a href="<?php print  $agent_id; ?>"><?php print t('Contact'); ?></a></div>
                             <div id="agent-id-<?php print  $agent_id ?>" class="dialog-form">
<?php 
     $form = module_invoke('webform', 'block_view', 'client-block-27');
     print '<div id="info"></div>'.$form['content'];
?>
</div>
                             <div class="office"><span>
                                <?php
                                if ($variables['office']) {
                                    print $variables['office']['buero'];
                                }
                                ?>
                            </span></div>
                             <div class="adress"> <?php
                                        if ($variables['office']) {
                                            print $variables['office']['address_street']
                                                    . $variables['office']['address_postalCode']
                                                    . $variables['office']['address_city'];
                                        }
                                        ?>
                                    </div>
                        </div>
                    </div>
                    <div class="details-container">
                       <div class="office-image">
                           <?php if ($variables['office']) {
                              $img=$variables['office']['pictureUrl'];
                              print '<img src="'.$img.'">';
                           }
                           ?>
                            </div>
                        <div class="office-info">
                            <div class="office"><span>
                                <?php
                                if ($variables['office']) {
                                    print $variables['office']['buero'];
                                }
                                ?>
                        </div>  
                            <br>
                            <a class="website-url" href="<?php print $variables['office']['websiteUrl'] ?>"><?php print t('View Local Website'); ?></a>
                            <br><br>
                            <div class="adress"><div><label><?php print t('Adress'); ?></label></div> <?php
                                        if ($variables['office']) {
                                            print $variables['office']['address_street']
                                                    . $variables['office']['address_postalCode']
                                                    . $variables['office']['address_city'];
                                        }
                                        ?>
                                    </div>
                            
                       </div>
                   
                </div></div></div>    
            <div id="tabs-3"><div class="agentDetailContent">
                    <div class="details-container">
                        <div class="min-info">
                            <div class="photo">
                                </div>
                                <h2><span class="name"><?php
                            if ($variables['agent']) {
                                print $variables['agent']['ip_usr_firstname'] . ' '
                                        . $variables['agent']['ip_usr_lastname'];
                            }
                            ?>
                                    </span></h2>
                             <div class="contact"><a href="<?php print  $agent_id; ?>"><?php print t('Contact'); ?></a></div>
                             <div id="agent-id-<?php print  $agent_id ?>" class="dialog-form">
                                             <?php 
                                              $form = module_invoke('webform', 'block_view', 'client-block-27');
                                              print '<div id="info"></div>'.$form['content'];
                                           ?>
                             </div>
                             <div class="office"><span>
                                <?php
                                if ($variables['office']) {
                                    print $variables['office']['buero'];
                                }
                                ?>
                            </span></div>
                             <div class="adress"> <?php
                                        if ($variables['office']) {
                                            print $variables['office']['address_street']
                                                    . $variables['office']['address_postalCode']
                                                    . $variables['office']['address_city'];
                                        }
                                        ?>
                                    </div>
                        </div>
                    </div>
                    </div> 
                    <?php
                    if ($variables['agent']) {
                        $email = $variables['agent']['ip_usr_email'];
                    
                    $view = views_get_view('real_estate_search');
                    $query = parse_url(request_uri());
                    if (empty($query['query'])) {
                        $view->set_exposed_input(array('agent' => $email));
                    }
                    $view->dom_id = "";
                    $view->override_path = $_GET['q'];

                    print $view->render('page_2');
                    }
                    ?>    
                
            
        </div>
    </div>
        </div>
    </div>
    <footer>     
        <div id="footerContent" class="footerContent">
            <div id="navPanel" class="footerRow">
                <?php
                $menu = menu_tree('menu-footer-navigation');
                echo render($menu);
                ?><div class="didYouKnow">
                    <div class="description">
                        <h3>DID YOU <span class="know">KNOW?</span></h3>
                        <span class="advancedOptions">
                            <strong>Advanced Options and Keyword Search </strong> let you search beyond beds, baths and price.
                        </span>
                    </div>
                </div>
                <div class="loginBox">

                </div>
            </div>
            <div id="footerBottomPanel" class="footerRow">
                <?php
                $menu = menu_tree('menu-footer-bottom-links');
                echo render($menu);
                ?>
                <?php print render($page['footer_second']); ?>            
            </div>
        </div>
    </footer>