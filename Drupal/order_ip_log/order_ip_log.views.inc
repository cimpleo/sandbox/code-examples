<?php
function order_ip_log_views_data() {
  $data = array();

  $data['order_ip_log']['table']['group'] = 'Ubercart order';

  $data['order_ip_log']['table']['base'] = array(
    'field' => 'ip',
    'title' => t("Order's ip log"),
    'help' => t("Order's ip log")
  );

  $data['order_ip_log']['table']['join']['uc_orders'] = array(
    'left_field' => 'order_id',
    'field' => 'order_id',
  );

  $data['order_ip_log']['ip'] = array(
    'title' => t("Orders's ip"),
    'help' => t("Orders's ip"),
    'field' => array(
      'handler' => 'views_handler_field'
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string'
    ),
    'sort' => array(
      'handler' => 'views_handler_sort'
    ),
  );

  $data['order_ip_log']['log_created'] = array(
    'title' => t("Log created"),
    'help' => t("Log created"),
    'field' => array(
      'handler' => 'views_handler_field_date'
    ),
    'filter' => array(
      'handler' => 'order_ip_log_views_handler_filter_log_created'
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date'
    ),
  );

  $data['order_ip_log']['ban_action'] = array(
    'title' => t("Ban/Unban action"),
    'help' => t("Ban/Unban action"),
    'field' => array(
      'handler' => 'views_handler_field_ban_actions'
    ),
  );

  $data['views']['ip_banned'] = array(
    'title' => t('Ip banned'),
    'help' => t('Ip banned'),
    'field'  => array(
      'handler' => 'order_ip_log_views_handler_field_ip_banned',
    ),
    'filter' => array(
      'handler' => 'order_ip_log_views_handler_filter_ip_banned'
    ),
  );

  $data['views']['log_date'] = array(
    'title' => t('Log date'),
    'help' => t('Log_date'),
    'field'  => array(
      'handler' => 'order_ip_log_views_handler_field_log_date',
    ),
    'sort' => array(
      'handler' => 'order_ip_log_views_handler_sort_log_date'
    ),
  );

  $data['views']['ip_count'] = array(
    'title' => t('Ip count'),
    'help' => t('Ip count'),
    'field'  => array(
      'handler' => 'order_ip_log_views_handler_field_ip_count',
    ),
  );

  return $data;
}

function order_ip_log_views_default_views() {
  $path = realpath(drupal_get_path('module', 'order_ip_log') . '/orders_ip_log_simple.view');
  require_once $path;
  $views[$view->name] = $view;

  $path = realpath(drupal_get_path('module', 'order_ip_log') . '/orders_ip_with_count.view');
  require_once $path;
  $views[$view->name] = $view;

  return $views;
}