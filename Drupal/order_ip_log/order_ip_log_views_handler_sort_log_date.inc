<?php
class order_ip_log_views_handler_sort_log_date extends views_handler_sort_date
{
  public function query() {
    $this->ensure_my_table();
    // Add the field.
    $this->query->add_orderby(NULL, NULL, $this->options['order'], $this->field);
  }

  public function options_form(&$form, &$form_state){
    parent::options_form($form, $form_state);

    unset($form['granularity']);


  }
}