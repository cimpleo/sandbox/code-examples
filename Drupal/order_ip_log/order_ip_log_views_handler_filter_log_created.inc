<?php
class order_ip_log_views_handler_filter_log_created extends views_handler_filter_date
{
//  public function init(&$view, $options) {
//    $options['value']['min'] = gmdate('Y-m-d 00:00', time());
//    $options['value']['max'] = gmdate('Y-m-d 23:59');
//   parent::init($view, $options);
//  }
  function option_definition() {
    $options = parent::option_definition();

    $options['value']['contains']['min']['default'] = gmdate('Y-m-d 00:00', time());
    $options['value']['contains']['max']['default'] = gmdate('Y-m-d 23:59', time());

    return $options;
  }

}