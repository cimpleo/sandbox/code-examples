(function () {
    'use strict';

    angular
        .module('hub')
        .controller('appProfileToolsInstanceController', appProfileToolsInstanceController);


    function appProfileToolsInstanceController($rootScope, $scope, $mdDialog, $state, $stateParams, appProfileDataService) {

        angular.extend($scope, {
            tempInstance: {},
            thisInstance: {},
            change : true,
            activeTools: [],
            setTempInstance: setTempInstance,
            setInstance: setInstance,
            createTool: createTool,
            editTool: editTool,
            showChooseToolDialog: showChooseToolDialog,
            showAddFailsDialog: showAddFailsDialog,
            checkChange: checkChange,
            deleteTool: deleteTool,
            deleteFail: deleteFail,
            cancelChanges: cancelChanges,
            deleteNewInstance: deleteNewInstance,
            refreshTool: refreshTool,
            getTools: getTools,
            showAddParamsDialog: showAddParamsDialog
        });


        function setInstance(obj){
            $scope.thisInstance = angular.copy(obj);
            setTempInstance($scope.thisInstance);
        }

        function setTempInstance(cb){
            $scope.tempInstance = angular.copy(cb);
            getActiveToolsId();
        }

        function cancelChanges(){
          $scope.tempInstance = angular.copy($scope.thisInstance);
          checkChange();
        }

        function createTool(item){
            appProfileDataService.createTool($scope.$parent.appId,item)
              .then(function(response){
                getTools($scope.thisInstance.id);
              });
        }

        function deleteNewInstance(key){
          $scope.$parent.instanceTools.splice(key,1);
        }

        function editTool(item){

          var tempItem = angular.copy(item),
           _instance = angular.copy(tempItem);

          delete _instance.tools;
          delete _instance.os;

          angular.forEach(tempItem.tools,function(value){

            let tempTool = angular.copy(value);
                tempTool.os = "any";
                tempTool.instance = angular.copy(_instance);

            appProfileDataService.editTool($scope.$parent.appId,tempTool)
              .then(function(response){
                $scope.thisInstance = angular.copy($scope.tempInstance);
                checkChange();
              });

          });
        }

      function getTools(cbid){
        appProfileDataService.getTools($scope.appId)
          .then(function(response){
            refreshTool(cbid,response.data);
          })
      }

      function refreshTool(cbid,array){
        var tempCodebase = {};

        array.forEach(function(value) {
          if (Object.keys(value).indexOf('instance') !== -1) {
            if (value.instance.id === cbid) {
              tempCodebase = angular.copy(value.instance);
            }
          }
        });

        tempCodebase.tools = [];

        angular.forEach(array,function(value){
          if (Object.keys(value).indexOf('instance') !== -1) {
            if (value.instance.id === cbid) {
              var temp_value = angular.copy(value);
              delete temp_value.instance;
              if (temp_value.os) {
                value.os = temp_value.os;
              }
              tempCodebase.tools.push(temp_value);
            }
          }
        });
        setInstance(tempCodebase);
      }

        function deleteTool(item){
          appProfileDataService.deleteTool($scope.$parent.appId,item)
            .then(function(response){
              var tempIndex = $scope.tempInstance.tools.findIndex(function(value){
                return value.id === item.id;
              });

              $scope.tempInstance.tools.splice(tempIndex,1);
              getActiveToolsId();
            });
        }

        function getActiveToolsId(){
          $scope.activeTools = [];
          if ($scope.tempInstance.tools.length) {
            angular.forEach($scope.tempInstance.tools ,function(value){
              $scope.activeTools.push(value.tool.id);
            });
          }
          console.log($scope.tempInstance.tools,$scope.activeTools);
        }

        function checkChange(){
           $scope.change = angular.equals($scope.tempInstance, $scope.thisInstance);
        }

        function deleteFail(mkey,key){
          $scope.tempInstance.tools[mkey].failedConditions.splice(key,1);
          checkChange();
        }

          function showChooseToolDialog() {
            $mdDialog.show({
              locals: {
                  appId: $scope.$parent.appId,
                  tools: $scope.$parent.toolsByWorkspace,
                  activeTools: $scope.activeTools,
                  type: 'instance'
              },
              clickOutsideToClose: false,
              templateUrl: 'app/appProfile/tools/dialogs/chooseTool.tmpl.html',
              controller: 'chooseToolController'
            }).then(function (answer) {
                var _instance = {
                  instance : angular.copy($scope.tempInstance),
                  tool : angular.copy(answer),
                  params : [],
                  failConditions : [],
                  os : $scope.tempInstance.os
                };
                createTool(_instance);
              },
              function () {
              }
            );
          }

      function showAddFailsDialog(item) {
        $mdDialog.show({
          locals: {
            appId: $scope.$parent.appId,
            fails: item,
            type: 'instance'
          },
          clickOutsideToClose: false,
          templateUrl: 'app/appProfile/tools/dialogs/tools.addFailsConditional.tmpl.html',
          controller: 'addFailsConditionalDialogController'
        }).then(function (answer) {
            checkChange();
          },
          function () {
            checkChange();
          }
        );
      }

      function showAddParamsDialog(item, params) {
        $mdDialog.show({
          locals: {
            appId: $scope.$parent.appId,
            params: $scope.$parent.toolType,
            tool: item,
            type: 'instance',
            selectedParams: params
          },
          clickOutsideToClose: false,
          templateUrl: 'app/appProfile/tools/dialogs/tools.params.tmpl.html',
          controller: 'addParamsDialogController'
        }).then(function (answer) {
            checkChange();
          },
          function () {
            checkChange();
          }
        );
      }
    }
})();