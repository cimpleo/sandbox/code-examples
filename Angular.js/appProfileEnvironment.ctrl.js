(function () {
    'use strict';

    angular
        .module('hub')
        .controller('appProfileEnvironmentController', appProfileEnvironmentController)
        .directive('scrollingTo', function() {
          return {
            restrict: 'A',
            link: function(scope, elm, attr){
              var scrollArea = $('#scrollTo'),
                  button = $('.create-item-button');

              $('.info-block').on('click','.create-item-button',function () {
              var _elem = $(this).parents('.info-block').find('.wrapper-codebase .item-codebase, .wrapper-instance .item-instance').last();
                _elem[0].scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});
                _elem.find('form input:first').focus();
              });
            }
          };
        });


    function appProfileEnvironmentController($rootScope, $scope, $mdDialog, $stateParams , appProfileDataService) {

        angular.extend($scope, {
          appId : $stateParams.appId,
          codebase: {},
          app_instance: {},
          permission:{
            codebase:{
              create: false,
              update: false,
              delete: false
            },
            app_instance:{
              create: false,
              update: false,
              delete: false
            }
          },
          loadScreen: {
            codebase: true,
            instance: true
          },
          getCodebase: getCodebase,
          getAppInstance: getAppInstance,
          showCreateCodebaseDialog: showCreateCodebaseDialog,
          showCreateInstanceDialog: showCreateInstanceDialog,
          addNewEmptyCodebase: addNewEmptyCodebase,
          addNewEmptyInstance: addNewEmptyInstance,
          deleteLastCodebase:  deleteLastCodebase,
          deleteLastInstance:  deleteLastInstance,
          checkIsNew: checkIsNew
        });

        getPermissions();
        getCodebase($scope.appId);
        getAppInstance($scope.appId);

        function getCodebase(appId){
          $scope.loadScreen.codebase = true;
          appProfileDataService.getCodebaseDetails(appId)
            .then(function(response){
              $scope.codebase = response.data;
              $scope.loadScreen.codebase = false;
            })
        }

        function getAppInstance(appId){
          $scope.loadScreen.instance = true;
          appProfileDataService.getAppInstanceDetails(appId)
            .then(function(response){
              $scope.app_instance = response.data;
              $scope.loadScreen.instance = false;
            })
        }

        function getPermissions(){
          getCBCreatePermisson();
          getCBDeletePermisson();
          getCBUpdatePermisson();

          getAICreatePermisson();
          getAIDeletePermisson();
          getAIUpdatePermisson();
        }

        function getCBCreatePermisson(){
          appProfileDataService.getPermisson('CREATE_CODEBASE')
            .then(function(response){
              $scope.permission.codebase.create = response.data;
            })
        }

        function getCBDeletePermisson(){
          appProfileDataService.getPermisson('DELETE_CODEBASE')
            .then(function(response){
              $scope.permission.codebase.delete = response.data;
            })
        }

        function getCBUpdatePermisson(){
          appProfileDataService.getPermisson('UPDATE_CODEBASE')
            .then(function(response){
              $scope.permission.codebase.update = response.data;
            })
        }

        function getAIUpdatePermisson(){
          appProfileDataService.getPermisson('UPDATE_APP_INS')
            .then(function(response){
              $scope.permission.app_instance.update = response.data;
            })
        }

        function getAICreatePermisson(){
          appProfileDataService.getPermisson('CREATE_APP_INS')
            .then(function(response){
              $scope.permission.app_instance.create = response.data;
            })
        }

        function getAIDeletePermisson(){
          appProfileDataService.getPermisson('DELETE_APP_INS')
            .then(function(response){
              $scope.permission.app_instance.delete = response.data;
            })
        }

        function checkIsNew(item){
          if (!item.id) {
            return true;
          } else {
            return false
          }
        }

        function addNewEmptyCodebase(){
          $scope.codebase.push({
            appId: $scope.appId,
            name: "",
            link: "",
            project: "",
            repo: "",
            branch: "master",
            vcsType: "git",
            checkoutRelativePath: "./",
            buildTool: "maven",
            active: true
          })
        }

        function addNewEmptyInstance(){
          $scope.app_instance.push({
            appId: $scope.appId,
            name: "",
            url: "",
            type: "",
            active: true,
            credentials: []
          })
        }

        function deleteLastCodebase(){
          deleteLastElement($scope.codebase);
        }

        function deleteLastInstance(){
          deleteLastElement($scope.app_instance);
        }

        function deleteLastElement(array){
          array.pop();
        }

      function showCreateCodebaseDialog() {
        $mdDialog.show({
          locals: {
            appId: $scope.appId
          },
          clickOutsideToClose: true,
          templateUrl: 'app/appProfile/environment/dialogs/create.codebase.tmpl.html',
          controller: 'createCodebaseController'
        }).then(function () {
          getCodebase($scope.appId);
        });
      }

      function showCreateInstanceDialog() {
        $mdDialog.show({
          locals: {
            appId: $scope.appId
          },
          clickOutsideToClose: true,
          templateUrl: 'app/appProfile/environment/dialogs/create.instance.tmpl.html',
          controller: 'createInstanceController'
        }).then(function () {
          getAppInstance($scope.appId);
        });
      }


        $scope.select_example5 = [
          "COTS",
          "Outsourced",
          "Inhouse Developed",
          "Contractor Developed",
          "Third Party’s",
          "Open Source"
        ];


    }
})();
