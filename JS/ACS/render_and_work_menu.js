//current page name (for navigation)
var page_title = $("html").attr('data-page-title').toLowerCase();

//connection errors counters for render and view menu
window.localStorage.setItem("journal_year_releases_error_count", "0");
window.localStorage.setItem("journal_year_releases_error_auth_count", "0");
window.localStorage.setItem("journal_year_releases_auth_error_fix_count", "0");

//error counters to record variables
var journal_year_releases_error_count = window.localStorage.getItem("journal_year_releases_error_count");
var journal_year_releases_error_auth_count = window.localStorage.getItem("journal_year_releases_error_auth_count");
var journal_year_releases_auth_error_fix_count = window.localStorage.getItem("journal_year_releases_auth_error_fix_count");

//Flag for making issues menu item active
window.localStorage.setItem("make_issues_menu_item_active", "yes");
window.localStorage.setItem("current_issue_info_received", "no");

//render_menu_years_items()
function render_menu_years_items() {  
  
  //Get year menu items
  var issue_years_list = JSON.parse(window.localStorage.getItem('menu-cache-year-menu-items'));
  
  //Clear year menu items
  $('.date-block-inner .item').remove();  
  
  //Hide menu year-prelouder
  $("#date-block-inner>#app-prelouder img, #date-block-inner>#app-prelouder").css({display: "none"});  
  
  //Print the years, the journal releases (the first item is made active)
  jQuery.each(issue_years_list, function (index, value) {   
    
    var class_for_item = "";

    if(index == 0) {
      class_for_item = "active";
    }
    
    $('.date-block-inner')
    .append(''
    + '<div class="item ' + class_for_item + '" data-url="' + value.url + '">'
      + value.label
    + '</div>'); 
          
    if ((index+1) == issue_years_list.length) {
      
      //if it's detail issue page or start page or home page, then search_active_year_menu_item()
      if (page_title.indexOf("detail") == 0 || page_title == "start_page" || page_title == "home_page") {
       
        var waiting_issue_info_timer_id = setTimeout(function waiting_issue_info() {
          if (window.localStorage.getItem("current_issue_info_received") == "no") {
            waiting_issue_info_timer_id = setTimeout(waiting_issue_info, 1000);
          } else {
            clearTimeout(waiting_issue_info_timer_id);
            //$('.date-block-inner .item:last-child').addClass('active');
            search_active_year_menu_item();
          }
        }, 1000);
      } else {   

        $(".date-block-inner .item").each(function(index, value) {       
          if(index == 0) {
            $(this).addClass('active');
            
            var url_part_for_render_issues_menu = $(this).attr('data-url');
          
            if (url_part_for_render_issues_menu.length > 0) {
                
              //get and print all the issues of the journal for the selected year
              var journal_year_url = window.localStorage.getItem("basic-api-url") + url_part_for_render_issues_menu;            
              get_journal_year_releases(journal_year_url); //func from render_and_work_menu.js                      
            } else {
      
              alert("Issues not found");
                            
            }
            
            return false
          }
        }) // END each
      };
    } //END if ((index+1) == issue_years_list.length)
  }); //END jQuery.each

} // END render_menu_years_items()

function view_menu_date_items_by_clicking_online() {  
  $('.wrapper').on('touchstart', '#nav .nav-inner .nav-blocks .date-block .date-block-inner .item', function () {
  
    $('.date-block-inner .item').removeClass('active');                  
    $(this).addClass('active');    

    //make issues menu item active (set flag for this)
    var click_current_year = $(this).text();
    
    if (Number(click_current_year) == Number(window.localStorage.getItem("current_issue_year"))) {                    
      window.localStorage.setItem("make_issues_menu_item_active", "yes");
    } else {                    
      window.localStorage.setItem("make_issues_menu_item_active", "no");
    }
                  
    var url_part_for_render_issues_menu = $(this).attr('data-url');
                  
    if (url_part_for_render_issues_menu.length > 0) {
                    
      $('.bottom-nav-block .menu-item').remove();
      //Show menu year-prelouder
      $(".bottom-nav-block>#app-prelouder img, .bottom-nav-block>#app-prelouder").css({display: "block"});
                    
      //get and print all the issues of the journal for the selected year
      var journal_year_url = window.localStorage.getItem("basic-api-url") + url_part_for_render_issues_menu;            
      get_journal_year_releases(journal_year_url); //func from render_and_work_menu.js
                    
    } else {
      
      alert("Issues not found");
                    
    }
                  
  }); //END touchstart                
} //END view_menu_date_items_by_clicking_online()

function search_active_year_menu_item() {
  
  $(".date-block-inner .item").removeClass("active");

  $(".date-block-inner .item").each(function() {  
    if($(this).text() == window.localStorage.getItem("current_issue_year")) {
      $(this).addClass('active');
          
      var url_part_for_render_issues_menu = $(this).attr('data-url');
          
      if (url_part_for_render_issues_menu.length > 0) {
          
        //get and print all the issues of the journal for the selected year
        var journal_year_url = window.localStorage.getItem("basic-api-url") + url_part_for_render_issues_menu;            
        get_journal_year_releases(journal_year_url); //func from render_and_work_menu.js                      
      }          
    };
  });
} //END search_active_year_menu_item()


function get_journal_year_releases(url) {

  var issue_menu_items_year = url[url.length - 4] + url[url.length - 3] + url[url.length - 2] + url[url.length - 1];  
  var groups = JSON.parse(window.localStorage.getItem('menu-cache-issue-menu-items-' + issue_menu_items_year + ''));   
  
  //Clear issue menu items
  $('.bottom-nav-block .menu-item').remove();
  
  var next_page = "detail_page.html";

  //Hide menu year-prelouder
  $(".bottom-nav-block>#app-prelouder img, .bottom-nav-block>#app-prelouder").css({display: "none"});

  jQuery.each(groups, function (index, value) { 
        $('.bottom-nav-block')
        .append(''
        + '<div class="menu-item" data-url="' + next_page + '?journal_url=' + value.url + '#someHashData">'
          + '<a>'
              + value.label
              //+ '<span class="page" style="color:red";> (pp. 279-342) </span>'
              //+ '<span class="date" style="color:red";>September 23. 2015</span>'
          + '</a>'
        + '</div>');
        
        if ((index+1) == groups.length && window.localStorage.getItem("make_issues_menu_item_active") == "yes") {
          search_active_issue_menu_item(groups.length);          
        }
  }); //END jQuery.each
      
  $('.nav-inner .nav-blocks .bottom-nav-block .menu-item').on('click', function () {      
    window.location.href = $(this).attr('data-url');        
  }); //END click

}; //End get_journal_year_releases()

function search_active_issue_menu_item(groups_length) {  
  
  if (page_title == "asap_articles" || page_title == "saved_articles") {
    return false
  }
  
  var counter = {};
  counter.value = 0;
  counter.active_menu_item_founded = false;
  
  //if it's detail issue page, then
  if (page_title.indexOf("detail") == 0) {
    
    var current_volume = window.localStorage.getItem("current_volume_number");
    var current_issue = window.localStorage.getItem("current_issue_number");
        

    $(".bottom-nav-block .menu-item").each(function() { 
      counter.value++;          
      var issue_menu_item_text = $(this).text();          
            
      if (issue_menu_item_text[5] == current_volume && issue_menu_item_text[12] == current_issue) {
        counter.active_menu_item_founded = true;
        $(this).addClass('active');
      } else if (counter.value == groups_length && !counter.active_menu_item_founded) {        
        update_app_menu_cache();
      }
    });
  } else {    
    $(".bottom-nav-block .menu-item").each(function(index, value) {       
      if(index == 0) {
        $(this).addClass('active');
        return false
      }
    })    
  };
} //END search_active_issue_menu_item()

//Navigation functions
function go_to_home_page_by_clicking_on_home_links() {
  $('.wrapper .top-block-wrap').on('touchstart', '.home-page-link', function () {
    window.location.href = "home_page.html";
  }); //END touchstart
}

function go_to_issue_by_clicking_on_arrow() {
  $('.wrapper>.content>.detail-wrap>.wtf-block>.wtf-inner>a#prev').on('touchstart', function () {    
    if ($(this).attr('data-url').length > 0) {
      //alert($(this).attr('data-url'))
      window.location.href = $(this).attr('data-url');
    }                            
  });

  $('.wrapper>.content>.detail-wrap>.wtf-block>.wtf-inner>a#next').on('touchstart', function () {                  
    if ($(this).attr('data-url').length > 0) {
      //alert($(this).attr('data-url'))
      window.location.href = $(this).attr('data-url');
    }                   
  });
}

function go_to_saved_and_asap_pages_by_clicking() {
  //Transfer to the page "Asap articles" get-parameters
  $('a#asap_articles').on('click', function () {
    //window.location.href = 'asap_articles.html?journal_url=' + journal_url + '&asap_url=' + data.quickLinks.aop[0].url + '#someHashData';
    window.location.href = 'asap_articles.html?journal_url=' + window.localStorage.getItem("url-of-basic-app-journal") + '&asap_url=' + window.localStorage.getItem("journal_link_to_asap_articles") + '#someHashData';  
  });

  //Transfer to the page "Saved issues and articles" get-parameters
  $('a#saved_articles').on('click', function () {
    //window.location.href = 'saved_articles.html?journal_url=' + journal_url + '&asap_url=' + data.quickLinks.aop[0].url + '#someHashData';
    window.location.href = 'saved_articles.html?journal_url=' + window.localStorage.getItem("url-of-basic-app-journal") + '&asap_url=' + window.localStorage.getItem("journal_link_to_asap_articles") + '#someHashData';
  });
}


















