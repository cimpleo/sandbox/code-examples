﻿function errorHandler() {

  this.redirect_to_offline_mode = function(errorMsgForAppUsers, errorMsgForDebugMode, xhrStatus, xhrSettings) {

    if (typeof errorMsgForAppUsers == "string" && typeof errorMsgForDebugMode == "string") {
      
      if (debug_mode) {
        var errorObject = {};
        errorObject.Current_page_name = page_title;
        errorObject.errorMsgForDebugMode = errorMsgForDebugMode;
        errorObject.xhrStatus = xhrStatus;
        errorObject.xhrSettings = xhrSettings;        
        showAlertDialogMsg(JSON.stringify(errorObject), redirectToOfflineMode, "Error message for developer", "Окay:(");
      } else {        
        showAlertDialogMsg(errorMsgForAppUsers, redirectToOfflineMode, "Error message", "Ок");        
      }

    } else {
      alert("Invalid arguments for errorHandlerObject (not string).")
    }
        
  };
  
  
  this.downloadIssueFilesErrorHandler = function(paths_to_garbage_files) {    
    var i = 0;
    while (i < paths_to_garbage_files.length) {
      delete_garbage_file(paths_to_garbage_files[i]); //from collector.js
      i++;
    }
    
    //delete_garbage_file(garbage_file_path);
    cleaning_issue_garbage_img_if_error_saving_issue(); //from collector.js
    cleaning_all_issue_garbage(); //from collector.js
  };
  
  
  
} // END Class errorHandler

function showAlertDialogMsg(message, callbackFunc, msgTitle, buttonName) {
  navigator.notification.alert(
      message,          // message
      callbackFunc,     // callback
      msgTitle,         // title
      buttonName        // buttonName
  ); 
}

function redirectToOfflineMode() {  
  window.location.href = "saved_articles_offline.html";
}
