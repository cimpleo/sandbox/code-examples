function cleaning_garbage_if_error_saving_article() {

  var path_to_garbage_article_picture = window.localStorage.getItem("article-img-local-path");

  var garbage_picture_name = path_to_garbage_article_picture.split('/');

  var file_name = garbage_picture_name[garbage_picture_name.length - 1];

  var platform_path;

  if (device.platform != "Android") {
    platform_path = cordova.file.documentsDirectory;
  } else {
    platform_path = cordova.file.externalDataDirectory;
  }

  window.resolveLocalFileSystemURL(platform_path, function (dir) {
    dir.getFile(file_name, {create: false}, function (fileEntry) {
      fileEntry.remove(function (file) {
        console.log("garbage picture removed");
        //Clear local storage
        window.localStorage.setItem("article-img-local-path", "");
      }, function () {
        console.log("Failed to delete garbage picture, file not found");
      }, function () {
        console.log("Failed to delete garbage picture, file not found");
      });
    }, function () {
      console.log("Failed to delete garbage picture, file not found");
    });
  });
} //END cleaning_garbage_if_error_saving_article ()

function cleaning_issue_garbage_img_if_error_saving_issue() {

  var saving_issue_now = JSON.parse(window.localStorage.getItem("saving-issue-now"));

  if ((typeof saving_issue_now.picture) !== "undefined" && saving_issue_now.picture !== "img/default-pic.jpg") {

    var path_to_garbage_issue_picture = saving_issue_now.picture;

    var garbage_picture_name = path_to_garbage_issue_picture.split('/');

    var file_name = garbage_picture_name[garbage_picture_name.length - 1];

    var platform_path;

    if (device.platform != "Android") {
      platform_path = cordova.file.documentsDirectory;
    } else {
      platform_path = cordova.file.externalDataDirectory;
    }

    window.resolveLocalFileSystemURL(platform_path, function (dir) {
      dir.getFile(file_name, {create: false}, function (fileEntry) {
        fileEntry.remove(function (file) {
          console.log("garbage issue picture removed");
        }, function () {
          console.log("Failed to delete garbage picture, file not found");
        }, function () {
          console.log("Failed to delete garbage picture, file not found");
        });
      }, function () {
        console.log("Failed to delete garbage picture, file not found");
      });
    });
  } //END removing issue garbage picture
}


function cleaning_all_issue_garbage() {

  var issue_tmp_entries = JSON.parse(window.localStorage.getItem("issue-entries"));

  if (issue_tmp_entries.entries.length > 0) {
    jQuery.each(issue_tmp_entries.entries, function (index, value) {

      var path_to_garbage_img = value.picture;
      var path_to_garbage_pdf = value.pdf;

      function delete_garbage_file(path) {

        if ((typeof path) !== "undefined" && path !== "img/default-pic.jpg" && path.length > 0) {

          var local_path_to_garbage_file = path.split("/");

          var garbage_file_name = local_path_to_garbage_file[local_path_to_garbage_file.length - 1];

          var platform_path;

          if (device.platform != "Android") {
            platform_path = cordova.file.documentsDirectory;
          } else {
            platform_path = cordova.file.externalDataDirectory;
          }

          window.resolveLocalFileSystemURL(platform_path, function (dir) {
            dir.getFile(garbage_file_name, {create: false}, function (fileEntry) {
              fileEntry.remove(function (file) {
                console.log("Garbage issue file removed");
              }, function () {
                console.log("Failed to delete garbage issue file, file not found");
              }, function () {
                console.log("Failed to delete garbage issue file, file not found");
              });
            }, function () {
              console.log("Failed to delete garbage issue file, file not found");
            });
          });
        }
      } //END delete_garbage_file(path)


      //Closure for dynamic deleting garbage files in a loop
      (function (path_to_garbage_img_right_now, path_to_garbage_pdf_right_now) {

        var deleting_img_path = path_to_garbage_img_right_now;
        var deleting_pdf_path = path_to_garbage_pdf_right_now;

        if ((typeof path_to_garbage_img) !== "undefined" && path_to_garbage_img !== "img/default-pic.jpg" && path_to_garbage_img.length > 0) {
          delete_garbage_file(deleting_img_path);
        }

        if ((typeof path_to_garbage_pdf) !== "undefined" && path_to_garbage_pdf.length > 0) {
          delete_garbage_file(deleting_pdf_path);
        }

      })(path_to_garbage_img, path_to_garbage_pdf);
      //END closure

    }); //END jQuery.each
  }

} //END cleaning_all_issue_garbage()


function delete_garbage_file(path) {

  if ((typeof path) !== "undefined" && path !== "img/default-pic.jpg" && path.length > 0) {

    var local_path_to_garbage_file = path.split("/");

    var garbage_file_name = local_path_to_garbage_file[local_path_to_garbage_file.length - 1];

    var platform_path;

    if (device.platform != "Android") {
      platform_path = cordova.file.documentsDirectory;
    } else {
      platform_path = cordova.file.externalDataDirectory;
    }

    window.resolveLocalFileSystemURL(platform_path, function (dir) {
      dir.getFile(garbage_file_name, {create: false}, function (fileEntry) {
        fileEntry.remove(function (file) {
          console.log("Garbage issue file removed");
        }, function () {
          console.log("Failed to delete garbage issue file, file not found");
        }, function () {
          console.log("Failed to delete garbage issue file, file not found");
        });
      }, function () {
        console.log("Failed to delete garbage issue file, file not found");
      });
    });
  }
} //END delete_garbage_file(path)





