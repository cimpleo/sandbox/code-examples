//Initialize page
var detail_page = {
  // Page Constructor
  initialize: function () {
    this.bindEvents();
  },
  // Bind Event Listeners
  //
  // Bind any events that are required on startup. Common events are:
  // 'load', 'deviceready', 'offline', and 'online'.
  bindEvents: function () {
    document.addEventListener('deviceready', this.onDeviceReady, false);
  },
  // deviceready Event Handler
  //
  // The scope of 'this' is the event. In order to call the 'receivedEvent'
  // function, we must explicitly call 'app.receivedEvent(...);'
  onDeviceReady: function () {   
    detail_page.receivedEvent('deviceready');
  },
  // Update DOM on a Received Event
  receivedEvent: function () {  
    //menu animation
    animation_menu();
    //END menu animation 

    //functions from render_and_work_menu.js
    go_to_home_page_by_clicking_on_home_links(); 
    go_to_issue_by_clicking_on_arrow(); 
    go_to_saved_and_asap_pages_by_clicking(); 
    //END functions from render_and_work_menu.js
  
    var connection = navigator.connection.type;

    if (connection == "none") {
      alert("You are offline now");
      //pdf viewer flag
      window.localStorage.setItem("after-pdf-view", "no");

      window.location.href = "start_page.html";
    } else {
      //FRONTEND
      //cover button animation (toggle)
      $('.content .detail-wrap .cover-btn-wrap .cover-btn').on('touchstart', function () {
        if ($(this).hasClass('up')) {
          $(this).removeClass('up').addClass('down').html('COLLAPSE COVER');
          $(".wtf-inner").addClass('wtf-inner-visible');
          $(".detail-wrap").addClass('detail-wrap-fix-height');

          return 
        }

        //touchstart-fix-overlay (to prevent accidental switching to another page)
        $("#touchstart-fix-overlay").fadeIn(100);
        $("#touchstart-fix-overlay").fadeOut(450);

        $(this).removeClass('down').addClass('up').html('EXPAND COVER');
        $(".wtf-inner").removeClass('wtf-inner-visible');
        $(".detail-wrap").removeClass('detail-wrap-fix-height');
      });
      //END cover button animation
      //END FRONTEND


      //BACKEND

      //error counters to record variables
      var journal_error_count = window.localStorage.getItem("journal_error_count");
      var journal_error_auth_count = window.localStorage.getItem("journal_error_auth_count");
      var journal_auth_error_fix_count = window.localStorage.getItem("journal_auth_error_fix_count");     

      var articles_only_fresh_release_error_count = window.localStorage.getItem("articles_only_fresh_release_error_count");
      var articles_only_fresh_release_error_auth_count = window.localStorage.getItem("articles_only_fresh_release_error_auth_count");
      var articles_only_fresh_release_auth_error_fix_count = window.localStorage.getItem("articles_only_fresh_release_auth_error_fix_count");

      init_detail_page();  
      
      function init_detail_page() {

        function parseGetParams() {
          var $_GET = {};
          var __GET = window.location.search.substring(1).split("&");
          for (var i = 0; i < __GET.length; i++) {
            var getVar = __GET[i].split("=");
            $_GET[getVar[0]] = typeof(getVar[1]) == "undefined" ? "" : getVar[1];
          }
          return $_GET;
        }

        var GET_param = parseGetParams();            
        
        var basic_journal_url_for_release = GET_param.journal_url.split("/volume:");
        var journal_url_for_release = basic_journal_url_for_release[0];
        var current_volume_and_issue = basic_journal_url_for_release[1].split("/issue:");
        
        var current_volume = current_volume_and_issue[0];
        var current_issue = current_volume_and_issue[1];
        
        function calculate_and_set_current_issue_year() {           
          var global_issue_year = window.localStorage.getItem("year_associated_with_global_current_volume");
          var global_issue_volume = window.localStorage.getItem("volume_associated_with_global_current_year");   
          var coefficient_for_calculation_current_year = Number(global_issue_volume) - Number(current_volume);     
          //Variable "current_issue_year" for work menu and select the active menu item                                   
          window.localStorage.setItem("current_issue_year", Number(global_issue_year) - coefficient_for_calculation_current_year);          
        }
        
        calculate_and_set_current_issue_year();

        window.localStorage.setItem("current_issue_number", current_issue);
        window.localStorage.setItem("current_volume_number", current_volume);

        var journal_url = window.localStorage.getItem("basic-api-url") + journal_url_for_release;
        var this_year_release_url = window.localStorage.getItem("basic-api-url") + GET_param.journal_url;

        var issue_local_name = "volume " + current_volume + ", issue " + current_issue;

        //print issue "label"
        $('.wrapper>.top-block-wrap>.article-title>p, .wrapper>.content>.detail-panel>.panel-left>.panel-header').text(issue_local_name);
        $('#detail-custom-menu-btn p').text(issue_local_name);

        //Set flag for use offline-cache (default value - "no");
        window.localStorage.setItem("use-offline-cache-on-detail-page", "no");
        //Set flag for use online-cache (default value - "no");
        window.localStorage.setItem("use-online-cache-on-detail-page", "no");
        
        var current_issue_label_id = 'Vol. ' + current_volume + ', No. ' + current_issue + ' (' + window.localStorage.getItem("current_issue_year") + ')';
        //Writing the "label" of the current issue of the journal in the local storage for its future store / delete, checks
        window.localStorage.setItem("current-issue-label-id", current_issue_label_id);
        
        var all_saved_issues = JSON.parse(window.localStorage.getItem("saved_issues"));
        window.localStorage.setItem("saved-issue-iter", "null");
        
        jQuery.each(all_saved_issues.issues, function (index, value) {          
          if (value.label_id == current_issue_label_id) {
            //Set flag for using offline-cache
            window.localStorage.setItem("use-offline-cache-on-detail-page", "yes");
            //Set index saved issue for offline
            window.localStorage.setItem("saved-issue-iter", index);
            return false
          } else if ((index + 1) == all_saved_issues.issues.length) {            
            window.localStorage.setItem("use-offline-cache-on-detail-page", "no");
            //Clear memory
            all_saved_issues = null;            
          }
        })//END jQuery.each(offline search)
        
        if (window.localStorage.getItem("use-offline-cache-on-detail-page") == "no") {
          var all_cached_issues = JSON.parse(window.localStorage.getItem("cached_issues_for_online"));
            
          jQuery.each(all_cached_issues.issues, function (index, value) {
            if (value.label_id == current_issue_label_id) {

              //flag for using cache
              window.localStorage.setItem("use-online-cache-on-detail-page", "yes");
              //Set index saved issue for offline
              window.localStorage.setItem("saved-issue-iter", index);
              return false
            } else if ((index + 1) == all_cached_issues.issues.length) {
              window.localStorage.setItem("use-online-cache-on-detail-page", "no");
            }
          })//END jQuery.each(online search)
        }

        
        if (window.localStorage.getItem("use-offline-cache-on-detail-page") == "no" && window.localStorage.getItem("use-online-cache-on-detail-page") == "no") {           
          get_journal("from_server"); 
        } else if (window.localStorage.getItem("use-online-cache-on-detail-page") == "yes") {           
          //call get_journal("from_online_cache") or get_journal("from_server")          
          var index_online_cached_issue = window.localStorage.getItem("saved-issue-iter");
           
          check_online_cache_of_current_issue(all_cached_issues, index_online_cached_issue, get_journal); //func from cache_storage.js          
        } else if (window.localStorage.getItem("use-offline-cache-on-detail-page") == "yes" && (all_saved_issues.issues[window.localStorage.getItem("saved-issue-iter")].next_issue) !== "undefined") {                   
          get_journal("from_offline_cache"); 
        } else {           
          //call get_journal("from_offline_cache") after updating issue info;
          update_and_print_offline_cache_of_current_issue(all_saved_issues, window.localStorage.getItem("saved-issue-iter"), get_journal); //func from cache_storage.js             
        } 
        
        
        //Flag for making issues menu item active
        window.localStorage.setItem("current_issue_info_received", "yes");

        //Render in the menu years (items) and menu issues (items) for the one year
        render_menu_years_items(); //func from render_and_work_menu.js        
                
        //Ajax-request all issues of the journal for the selected year by clicking(touchstart)
        view_menu_date_items_by_clicking_online(); //func from render_and_work_menu.js
        
        //Requesting all information about the issue
        function get_journal(current_issue_source) {

          //Clear cache for rendering current issue page
          window.localStorage.setItem('articles_to_load', '{"articles":[]}');
          window.localStorage.setItem('research_articles_to_load', '{"research_articles":[]}');        
          
          if(current_issue_source == "from_server") {
            
            $.ajax({
              type: 'GET',
              url: journal_url,
              headers: {
                "Content-Type": "application/json",                
                "ACCESS-TOKEN": window.localStorage.getItem("access_token"),
                "accessInfo": "false"
              },

              success: function (data) {
                //Create storage required for online issue caching 
                window.localStorage.setItem("cached-issue-entries", '{"entries":[]}');

                //reset counter connection error
                window.localStorage.setItem("journal_error_count", "0");

                //print the current issue articles and set flag "current_issue_year" for making menu item ACTIVE
                get_articles_only_fresh_release();    

                function get_articles_only_fresh_release() {
                  $.ajax({
                    type: 'GET',
                    url: this_year_release_url,
                    headers: {
                      "Content-Type": "application/json",                      
                      "ACCESS-TOKEN": window.localStorage.getItem("access_token"), 
                      "accessInfo": "false"
                    },

                    success: function (data) {

                      //reset counter connection error
                      window.localStorage.setItem("articles_only_fresh_release_error_count", "0");
                      
                      //The arrow (left and right) to assign get-parameters for the go to the other issues of the journal
                      var prev_issue = data.previous;
                      var next_issue = data.next; 

                      set_url_attr_for_nav_arrows(prev_issue, next_issue);

                      get_journal_from_online_server();

                      function get_journal_from_online_server() {                        

                        var path_to_issue_picture;
                        var img_prelouder_time = 1550;
                        
                        if ((typeof data.cover) == "undefined") {
                          
                          if((typeof data.title) == "undefined") {                            
                            showAlertDialogMsg("Connection error", redirectToOffline, "Error message", "Ок");
                            return false;
                          } else {
                            path_to_issue_picture = "img/test-announces.jpg";
                          }

                          function redirectToOffline() {  
                            window.location.href = "saved_articles_offline.html";
                          }
                          
                        } else {
  
                          if (screen.width >= 768 ) {
                            img_prelouder_time = 11550;
                            path_to_issue_picture = "http://pubs.acs.org/subscribe/covers/acscii/acscii_v00" + current_volume + "i00" + current_issue + ".jpg"; 
                          } else {
                            path_to_issue_picture = data.cover;
                          }
                          
                        };                      
                                            
                        //Writing in the layout information to save the issue
                        $(".content>.detail-wrap").attr("data-issue-picture", path_to_issue_picture);                        
                        $(".content>.detail-wrap").attr("data-issue-local-name", issue_local_name);
                        $(".content>.detail-wrap").attr("data-issue-title", data.title);
                        $(".content>.detail-wrap").attr("data-issue-url", data.url);
                        $(".content>.detail-wrap").attr("data-issue-date", data.date);  

                        //issue img convert in the issue background
                        $('.for-img-block').css({backgroundImage:'url("' + path_to_issue_picture + '")'});

                        setTimeout(function () {
                          $(".detail-wrap #app-prelouder img").css({display: "none"})
                        }, img_prelouder_time);

                        //Here begins the conclusion of issue articles

                        //check articles existing
                        if ((typeof data.entries) == "undefined") {

                          $(".announces-wrap-2 #app-prelouder img, .announces-wrap-2 #app-prelouder").css({display: "none"});
                          $('#news_and_views').html('</br><p style="display:block; text-align:center;">Articles not found.</p></br>');
                          $('#research_articles').html('</br><p style="display:block; text-align:center;">Research articles not found.</p></br>');
                          //Clear cache
                          window.localStorage.setItem('articles_to_load', '{"articles":[]}');
                          window.localStorage.setItem('research_articles_to_load', '{"research_articles":[]}');                          
                          //switch on issue saving functionality
                          $('.content .detail-panel .panel-right #save-click-block').addClass("saving-functionality-ready");

                        } else if (data.entries.length == 0) {

                          $(".announces-wrap-2 #app-prelouder img, .announces-wrap-2 #app-prelouder").css({display: "none"});
                          $('#news_and_views').html('</br><p style="display:block; text-align:center;">Articles not found.</p></br>');
                          $('#research_articles').html('</br><p style="display:block; text-align:center;">Research articles not found.</p></br>');
                          //Clear cache
                          window.localStorage.setItem('articles_to_load', '{"articles":[]}');
                          window.localStorage.setItem('research_articles_to_load', '{"research_articles":[]}');                          
                          //switch on issue saving functionality
                          $('.content .detail-panel .panel-right #save-click-block').addClass("saving-functionality-ready");

                        }

                        //if the articles have a wrappers, then count the number of articles
                        if (data.entries[0].entries) {
                          var articles_iter = 0;
                          jQuery.each(data.entries, function (index, value) {
                            //"... && (typeof value.title) !== "undefined")" - "Debag code to filter articles"
                            if ((typeof value.entries) == "object" && (typeof value.title) !== "undefined") {
                              jQuery.each(value.entries, function (subindex, subvalue) {
                                articles_iter++;
                                window.localStorage.setItem("article_count", articles_iter);
                              }); //END jQuery.each
                            }
                          }); //END jQuery.each

                          create_cache_for_view_and_crud_of_current_issue(data.entries, current_issue_source);
                          
                        } else {
                          alert("Articles not found");
                          alert("Connection error, please try again later.");
                          window.location.href = "home_page.html";
                        }
                      } //END get_journal_from_online_server()


                      //END the conclusion of issue articles
                    },

                    error: function (xhr,data,settings) {

                      if (xhr.status == 401) {
                        if (articles_only_fresh_release_error_auth_count != retry_count) {
                          articles_only_fresh_release_error_auth_count++;
                          window.localStorage.setItem("articles_only_fresh_release_error_auth_count", articles_only_fresh_release_error_auth_count);
                          get_articles_only_fresh_release_auth_error_fix();
                        } else {
                          var currentErrorHandler = new errorHandler(); //from app_error_handlers.js              
                          currentErrorHandler.redirect_to_offline_mode("Connection error, please try again later.","Authentication error. Function - get_articles_only_fresh_release()", xhr.status, settings);
                        }
                      } else {
                        if (articles_only_fresh_release_error_count != retry_count) {
                          articles_only_fresh_release_error_count++;
                          window.localStorage.setItem("articles_only_fresh_release_error_count", articles_only_fresh_release_error_count);
                          get_articles_only_fresh_release();
                        } else {
                          var currentErrorHandler = new errorHandler(); //from app_error_handlers.js              
                          currentErrorHandler.redirect_to_offline_mode("Connection error, please try again later.","Not authentication error. Function - get_articles_only_fresh_release()", xhr.status, settings);
                        }
                      };

                    }

                  }); //End ajax-request fresh issue
                }; //End get_only_fresh_release()
                
                function get_articles_only_fresh_release_auth_error_fix() {
                  $.ajax({
                    type: 'PUT',
                    url: session_url,
                    data: JSON.stringify(authentication_json),
                    processData: false,
                    contentType: 'application/json',

                    success: function (data) {
                      window.localStorage.setItem("articles_only_fresh_release_auth_error_fix_count", "0");
                      //window.localStorage.setItem("cookie", data.cookie.value);
                      window.localStorage.setItem("access_token", data.access_token);
                      get_articles_only_fresh_release();
                    },

                    error: function (xhr,data,settings) {
                      if (articles_only_fresh_release_auth_error_fix_count != retry_count) {
                        articles_only_fresh_release_auth_error_fix_count++;
                        window.localStorage.setItem("articles_only_fresh_release_auth_error_fix_count", articles_only_fresh_release_auth_error_fix_count);
                        get_articles_only_fresh_release_auth_error_fix();
                      } else {
                        var currentErrorHandler = new errorHandler(); //from app_error_handlers.js              
                        currentErrorHandler.redirect_to_offline_mode("Connection error, please try again later.","Connection error. Function - get_articles_only_fresh_release_auth_error_fix()", xhr.status, settings);
                      }

                    }
                  });
                };


 
              }, //END Success

              error: function (xhr,data,settings) {
                if (xhr.status == 401) {
                  if (journal_error_auth_count != retry_count) {
                    journal_error_auth_count++;
                    window.localStorage.setItem("journal_error_auth_count", journal_error_auth_count);
                    get_journal_auth_error_fix(current_issue_source);
                  } else {
                    var currentErrorHandler = new errorHandler(); //from app_error_handlers.js              
                    currentErrorHandler.redirect_to_offline_mode("Connection error, please try again later.","Authentication error. Function - get_journal()", xhr.status, settings);
                  }
                } else {
                  if (journal_error_count != retry_count) {
                    journal_error_count++;
                    window.localStorage.setItem("journal_error_count", journal_error_count);
                    get_journal(current_issue_source);
                  } else {
                    var currentErrorHandler = new errorHandler(); //from app_error_handlers.js              
                    currentErrorHandler.redirect_to_offline_mode("Connection error, please try again later.","Not authentication error. Function - get_journal()", xhr.status, settings);
                  }
                };
              } //END Error


            }); // End ajax-request information about the issue
          
          } else {   
            
            $(".detail-wrap #app-prelouder img").css({display: "none"});

            var cached_issue_index = window.localStorage.getItem("saved-issue-iter");
            
            var all_saved_issues;

            if (current_issue_source == "from_offline_cache") {              
              
              all_saved_issues = JSON.parse(window.localStorage.getItem("saved_issues"));
              
              //Frontend-fix for using device back-button and app save-button
              $('.wrapper>.content>.detail-panel>.panel-right>.panel-right-inner>.panel-header').text('Delete issue');
              $('#save-click-block').addClass('on');
              $(".wrapper>.content>.detail-panel>.panel-right>.panel-right-inner>.save-btn-wrap>.save-btn-wrap-inner").html('<input type="checkbox" name="toggle" id="toggle" class="btn-label off"><label for="toggle"></label>');
              $('#toggle').click();
            } else if(current_issue_source == "from_online_cache") {                           
              all_saved_issues = JSON.parse(window.localStorage.getItem("cached_issues_for_online"));                             
            }
            
            var cached_issue = all_saved_issues.issues[cached_issue_index];            
            all_saved_issues = null;
            
            //issue img convert in the issue background
            $('.for-img-block').css({
              backgroundImage: function () {
                return 'url("' + cached_issue.picture + '")';
              }
            });
            
            var prev_issue = cached_issue.prev_issue;
            var next_issue = cached_issue.next_issue; 

            set_url_attr_for_nav_arrows(prev_issue, next_issue);
            
            //Writing in the layout information to save the issue
            $(".content>.detail-wrap").attr("data-issue-picture", cached_issue.server_picture);                        
            $(".content>.detail-wrap").attr("data-issue-local-name", cached_issue.issue_local_name);
            $(".content>.detail-wrap").attr("data-issue-title", cached_issue.title);
            $(".content>.detail-wrap").attr("data-issue-url", cached_issue.url);
            $(".content>.detail-wrap").attr("data-issue-date", cached_issue.date);
            
            var issue_articles = cached_issue.entries;
            
            if (issue_articles.length !== 0) {

              //Flag of existing research articles
              //window.localStorage.setItem("ResearchArticlesExist", "no");

              //record the length of the array of articles
              window.localStorage.setItem("article_count", issue_articles.length);

              prepare_tmp_articles_for_view(issue_articles, current_issue_source);
              
            } else {
              //switch on issue saving functionality
              $('.content .detail-panel .panel-right #save-click-block').addClass("saving-functionality-ready");
              $('#news_and_views').html('</br><p style="display:block; text-align:center;">Articles not found.</p></br>');
              $('#research_articles').html('</br><p style="display:block; text-align:center;">Research articles not found.</p></br>');
            }

          } 

        } // END get_journal()
          
        function get_journal_auth_error_fix(current_issue_source) {
          $.ajax({
            type: 'PUT',
            url: session_url,
            data: JSON.stringify(authentication_json),
            processData: false,
            contentType: 'application/json',

            success: function (data) {
              window.localStorage.setItem("fresh_releases_auth_error_fix_count", "0");
              //window.localStorage.setItem("cookie", data.cookie.value);
              window.localStorage.setItem("access_token", data.access_token);              
              get_journal(current_issue_source);
            },

            error: function (xhr,data,settings) {
              if (journal_auth_error_fix_count != retry_count) {
                journal_auth_error_fix_count++;
                window.localStorage.setItem("journal_auth_error_fix_count", journal_auth_error_fix_count);
                get_journal_auth_error_fix(current_issue_source);
              } else {
                var currentErrorHandler = new errorHandler(); //from app_error_handlers.js              
                currentErrorHandler.redirect_to_offline_mode("Connection error, please try again later.","Connection error. Function - get_journal_auth_error_fix()", xhr.status, settings);
              }
            }
          });
        }; //END get_journal_auth_error_fix()
          
        
      } // End init_detail_page      

    } // End connection

  } // End receivedEvent

}; // End detail_page(object)


function create_cache_for_view_and_crud_of_current_issue(articles, current_issue_source) {                          
     
  jQuery.each(articles, function (index, value) {
    
    //"... && (typeof value.title) !== "undefined")" - "Debag code to filter articles"
    if ((typeof value.entries) == "object" && (typeof value.title) !== "undefined") {
      //delete spaces in the begin and end of article tag
      article_tag = $.trim(value.title);
      //delete html-tags from title
      article_tag = article_tag.replace(/<\/?[^>]+>/g, '');
      
      prepare_tmp_articles_for_view(value.entries, current_issue_source, article_tag);     
      
    } 
  }); //END jQuery.each

}

function prepare_tmp_articles_for_view(filtered_articles, current_issue_source, article_tag) {
  
  
  jQuery.each(filtered_articles, function (index, value) {
    
    var id = value.id;    
    var title = value.title.replace(/<\/?[^>]+>/g, ''); //delete html-tags from title    
    var tag;
    var author;
    var picture;
    var pdf;
    var pdf_active_view_link;
    var path_to_pdf_on_server = "";
    var server_picture = "";
        
    if(current_issue_source == "from_server") {
      tag = article_tag; 

      //check article author
      if ((typeof value.contributors) == "undefined") {
        author = "";
      } else {
        author = value.contributors[0].name;
      };

      //check article picture
      if ((typeof value.figure) == "undefined") {
        picture = "img/test-announces.jpg";
      } else {
        picture = window.localStorage.getItem("basic-api-url") + value.figure.large.url;
      };
      
      pdf = window.localStorage.getItem("basic-api-url") + value.files[0].bookmarkUrl;
      pdf_active_view_link = pdf.replace("/pdf/", "/ipdf/");
      
    } else {
      tag = value.tag;
      author = value.author;
      picture = value.picture;
      pdf = value.pdf;
      pdf_active_view_link = value.pdf_active_view_link;
      
      if (window.localStorage.getItem("use-offline-cache-on-detail-page") == "yes") {
        path_to_pdf_on_server = value.path_to_pdf_on_server;
      }
      
      server_picture = value.server_picture;
      
    }
     
    //closing for using local variables
    (function (id, title, tag, author, picture, pdf, pdf_active_view_link, path_to_pdf_on_server, server_picture, current_issue_source) {
      var id = id;
      var title = title;
      var author = author;
      var picture = picture;
      var pdf = pdf;
      var pdf_active_view_link = pdf_active_view_link;
      var path_to_pdf_on_server = path_to_pdf_on_server;
      var server_picture = server_picture;
      var current_issue_source = current_issue_source;
       
      //Save all articles in the local storage
      save_tmp_articles_for_view(id, title, tag, author, picture, pdf, pdf_active_view_link, path_to_pdf_on_server, server_picture, current_issue_source);
    })(id, title, tag, author, picture, pdf, pdf_active_view_link, path_to_pdf_on_server, server_picture, current_issue_source);
    //END closing

  }); //END jQuery.each
} // END prepare_tmp_articles_for_view()

var articles_iter = 0;

function save_tmp_articles_for_view(id, title, tag, author, picture, pdf, pdf_active_view_link, path_to_pdf_on_server, server_picture, current_issue_source) {
                           
  var articleInfo = {};
  articleInfo.id = id;
  articleInfo.title = title;
  articleInfo.tag = tag;
  articleInfo.author = author;
  articleInfo.picture = picture;        
  articleInfo.pdf = pdf;
  articleInfo.pdf_active_view_link = pdf_active_view_link;
  
  if (current_issue_source == "from_offline_cache") {
    articleInfo.pdf = path_to_pdf_on_server;   
    articleInfo.server_picture = picture;
    articleInfo.picture = server_picture;
  }
  
  if (tag.toLowerCase() !== "research articles") {
    //Save the article in a temporary array "articles_to_load"
    var articles_to_load = JSON.parse(window.localStorage.getItem("articles_to_load"));

    //add a new article to the end of the array "articles_to_load"
    articles_to_load.articles.push(articleInfo);

    //rewrite temporary array "articles_to_load"
    window.localStorage.setItem('articles_to_load', JSON.stringify(articles_to_load));
                              
  } else if (tag.toLowerCase() == "research articles") {
    //Save the article in a temporary array "research_articles_to_load"
    var research_articles_to_load = JSON.parse(window.localStorage.getItem("research_articles_to_load"));

    //add a new article to the end of the array "research_articles_to_load"
    research_articles_to_load.research_articles.push(articleInfo);

    //rewrite temporary array "research_articles_to_load"
    window.localStorage.setItem('research_articles_to_load', JSON.stringify(research_articles_to_load));
  } 

  if (tag.length > 0 && current_issue_source == "from_server") {    
    //add a new article to the end of the array "cached-issue-entries"
    var entries_for_online_cashing_issue = JSON.parse(window.localStorage.getItem("cached-issue-entries"));                            
    entries_for_online_cashing_issue.entries.push(articleInfo);
    //rewrite temporary array "cached-issue-entries"
    window.localStorage.setItem('cached-issue-entries', JSON.stringify(entries_for_online_cashing_issue));
    entries_for_online_cashing_issue = {};
  }
 
  articles_iter++;
  
  if (articles_iter == window.localStorage.getItem("article_count")) {
    //clean memory
    articleInfo = {};
    
    if (current_issue_source == "from_server") {    
      //Add the current issue in an online cache
      add_issue_to_online_cache_for_detail_page(); //func form cache_storage.js
    }
                              
    show_articles(current_issue_source);
  }
} //END save_tmp_articles_for_view()

function set_url_attr_for_nav_arrows(prev_issue, next_issue) {                        
        
  var next_page = "detail_page.html";
        
  if ((typeof prev_issue) == "undefined" || prev_issue == "undefined") {
    $('.wtf-block #prev').css({display: 'none'});
    $(".content>.detail-wrap").attr("data-issue-prev", "undefined");
  } else {
    
    var prev_issue_url;
    
    if(window.localStorage.getItem("use-offline-cache-on-detail-page") == "yes" || window.localStorage.getItem("use-online-cache-on-detail-page") == "yes") {
      prev_issue_url = prev_issue;
    } else {
      prev_issue_url = prev_issue.url;
    }

    $(".content>.detail-wrap").attr("data-issue-prev", prev_issue_url);
    $('.wrapper>.content>.detail-wrap>.wtf-block>.wtf-inner>a#prev').attr("data-url", next_page + '?journal_url=' + prev_issue_url + '#someHashData'); 
  };
                        
  if ((typeof next_issue) == "undefined" || next_issue == "undefined") {
    $('.wtf-block #next').css({display: 'none'});
    $(".content>.detail-wrap").attr("data-issue-next", "undefined");
  } else {
    
    var next_issue_url = next_issue.url;
    
    if(window.localStorage.getItem("use-offline-cache-on-detail-page") == "yes" || window.localStorage.getItem("use-online-cache-on-detail-page") == "yes") {
      next_issue_url = next_issue;
    } else {
      next_issue_url = next_issue.url;
    }

    $(".content>.detail-wrap").attr("data-issue-next", next_issue_url);
    $('.wrapper>.content>.detail-wrap>.wtf-block>.wtf-inner>a#next').attr("data-url", next_page + '?journal_url=' + next_issue_url + '#someHashData');                        
  };
                        
} // END set_url_attr_for_nav_arrows()

function show_articles(current_issue_source) {
  
  //switch on issue saving functionality
  $('.content .detail-panel .panel-right #save-click-block').addClass("saving-functionality-ready");
  $(".announces-wrap-2 #app-prelouder img, .announces-wrap-2 #app-prelouder").css({display: "none"});
  
  //auxiliary variable to load articles by scroll  
  var articles_quota = 15;
  
  if (window.localStorage.getItem("articles_to_load") !== '{"articles":[]}') {
    //get articles from temporary array "articles_to_load"
    var articles_to_load = JSON.parse(window.localStorage.getItem("articles_to_load"));
    print_first_articles(articles_to_load.articles);
  } else {
    $('#news_and_views').html('</br><p style="display:block; text-align:center";>Articles not found.</p></br>');
  }

  if (window.localStorage.getItem("research_articles_to_load") !== '{"research_articles":[]}') {
    //get research articles from temporary array "research_articles_to_load"
    var research_articles_to_load = JSON.parse(window.localStorage.getItem("research_articles_to_load"));
     print_first_articles(research_articles_to_load.research_articles);
  } else {    
    $('#research_articles').html('</br><p style="display:block; text-align:center";>Research articles not found.</p></br>');
  }

  function print_first_articles(articles_obj) {

    //set flag for load research articles by scrolling
    window.localStorage.setItem("research_art_ended", "false");

    var articles_first_print_quota;

    if (articles_obj[0].tag.toLowerCase() !== "research articles") {
      articles_first_print_quota = articles_quota;

      //Set flag for print research articles
      if (articles_obj.length <= articles_first_print_quota) {
        window.localStorage.setItem("print_research_art_flag", "true");
      } else {
         window.localStorage.setItem("print_research_art_flag", "false");
      }

    } else {
      articles_first_print_quota = articles_quota;
    }


    jQuery.each(articles_obj, function (subindex, subvalue) {

      if (subindex == articles_first_print_quota) {
        return false
      }

      var article_tag;
      var article_id = subvalue.id;
      var article_title = subvalue.title;
      var article_author = subvalue.author;
      var picture = subvalue.picture;
      var view_picture_path = picture;
      
      var server_article_picture_path = picture;

      if (current_issue_source == "from_offline_cache") {
        view_picture_path = subvalue.server_picture;
        server_article_picture_path = subvalue.picture;
      }

      var article_pdf_url = subvalue.pdf;
      var pdf_active_view_link = subvalue.pdf_active_view_link;                          
                                
      var container_for_articles;

      //check article tag
      if (subvalue.tag.toLowerCase() !== "research articles") {
        container_for_articles = "#news_and_views";
        article_tag = subvalue.tag;        
      } else {
        container_for_articles = "#research_articles";
        article_tag = "research article";
      };

      $(container_for_articles)
      .append(''
      + '<div class="more-item  article-item" data-article-tag="' + article_tag + '" data-article-pdf="' + article_pdf_url + '" data-pdf-active-view-link="' + pdf_active_view_link + '" data-article-title="' + article_title + '"data-article-author="' + article_author + '"data-article-picture="' + server_article_picture_path + '"data-article-id="' + article_id + '">'
        + '<div class="photo">'
          + '<img src="' + view_picture_path + '" />'
        + '</div>'
        + '<div class="text-block">'
          + '<div class="text-title">'
            + article_tag
          + '</div>'
          + '<div class="text-content">'
            + article_title
          + '</div>'
           + '<div class="text-author">'
             + article_author
          + '</div>'
        + '</div>'
      + '</div>');

      $('.announces-wrap-2 .more-item .photo').css({
        backgroundImage: function () {
          var imageSrc = $('img', $(this)).attr('src');
          return 'url("' + imageSrc + '")';
        }
      });

    }); //END jQuery.each

  } //END print_first_articles(array)

  //Flag for getting issue articles
  window.localStorage.setItem("takeArticlesFlag", "true");

  //auxiliary variables to load articles by scroll
  window.localStorage.setItem("articles_quota_number", articles_quota);
  window.localStorage.setItem("research_articles_quota_number", articles_quota);

  $(window).scroll(function () {
    //Flag for getting issue articles
    var takeArticles = window.localStorage.getItem("takeArticlesFlag");

    //Scroll calculation. If scroll to the bottom of the page, then
    if ($(window).scrollTop() + $(window).height() > $(document).height() - 700 && takeArticles == "true") {

      print_more_articles();

      function print_more_articles() {

        //Create an array of storage elements loadable by scroll
        var elems = [];

        //get articles from temporary array
        var articles_to_load;

        //auxiliary variable to load articles by scroll
        var articles_quota_number;

        //Remove from the array of those articles that are already loaded on the page
        var load_articles;

        //Get flag for print research articles
        var print_research_art_flag = window.localStorage.getItem("print_research_art_flag");

        if (print_research_art_flag == "false") {

          articles_to_load = JSON.parse(window.localStorage.getItem("articles_to_load"));
          articles_quota_number = window.localStorage.getItem("articles_quota_number");
          load_articles = $(articles_to_load.articles).slice(articles_quota_number);

        } else {

          articles_to_load = JSON.parse(window.localStorage.getItem("research_articles_to_load"));
          if (articles_to_load.research_articles.length > 0) {
            articles_quota_number = window.localStorage.getItem("research_articles_quota_number");
            load_articles = $(articles_to_load.research_articles).slice(articles_quota_number);
          } else {
            load_articles = articles_to_load.research_articles;
          }
        }
        
        if (load_articles.length > 0) {

          jQuery.each(load_articles, function (subindex, subvalue) {
            if (subindex == articles_quota) {
              return false
            }

            var article_tag;
            var article_id = subvalue.id;
            var article_title = subvalue.title;
            var article_author = subvalue.author;
            var picture = subvalue.picture;
            var view_picture_path = picture;
      
            if (current_issue_source == "from_offline_cache") {
              view_picture_path = subvalue.server_picture;
            }
            
            var article_pdf_url = subvalue.pdf;
            var pdf_active_view_link = subvalue.pdf_active_view_link;                                  
                                
            var container_for_articles;

            //check article tag
            if (subvalue.tag.toLowerCase() !== "research articles") {
              container_for_articles = "#news_and_views";
              article_tag = subvalue.tag;              
            } else {
              container_for_articles = "#research_articles";
              article_tag = "research article";
            };
            
            var div = $('<div/>')
            .append(''
            + '<div class="more-item  article-item" data-article-tag="' + article_tag + '" data-article-pdf="' + article_pdf_url + '" data-pdf-active-view-link="' + pdf_active_view_link + '" data-article-title="' + article_title + '"data-article-author="' + article_author + '"data-article-picture="' + picture + '"data-article-id="' + article_id + '">'
              + '<div class="photo">'
                + '<img src="' + view_picture_path + '" />'
              + '</div>'
              + '<div class="text-block">'
                + '<div class="text-title">'
                  + article_tag
                + '</div>'
                + '<div class="text-content">'
                  + article_title
                + '</div>'
                  + '<div class="text-author">'
                    + article_author
                  + '</div>'
              + '</div>'
            + '</div>');

            var elem = div.children();
            elems.push(elem);

            if (elems.length == articles_quota) {
              $(container_for_articles).append(elems);
            } else if (load_articles.length < articles_quota && elems.length < articles_quota && elems.length > 0) {
              $(container_for_articles).append(elems);
            }

          });

          //articles img convert in the articles background
          $('.announces-wrap-2 .more-item .photo').css({
            backgroundImage: function () {
              var imageSrc = $('img', $(this)).attr('src');
              return 'url("' + imageSrc + '")';
            }
          });

          //Flag for getting articles
          window.localStorage.setItem("takeArticlesFlag", "true");

          //rewrite counter to load content
          var counter;

          if (window.localStorage.getItem("print_research_art_flag") == "false") {
            counter = window.localStorage.getItem("articles_quota_number");
            counter = Number(counter) + Number(articles_quota);
            window.localStorage.setItem("articles_quota_number", counter);
          } else {
            counter = window.localStorage.getItem("research_articles_quota_number");
            counter = Number(counter) + Number(articles_quota);
            window.localStorage.setItem("research_articles_quota_number", counter);
          }

        } else {

          if (window.localStorage.getItem("research_art_ended") == "true") {
            window.localStorage.setItem("takeArticlesFlag", "false");
          } else {
            //Set flag for print research articles
            window.localStorage.setItem("print_research_art_flag", "true");
            //set flag for load research articles by scrolling
            window.localStorage.setItem("research_art_ended", "true");
            print_more_articles();
          }

        }

      } //end print_more_articles()
    } //end scroll calculation
  }); //end scroll
} //end show_articles()




